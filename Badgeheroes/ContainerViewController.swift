//
//  ViewController.swift
//  copyapp
//
//  Created by Gonzalo Lopez on 5/16/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import UIKit
import FontAwesome_swift
import SwiftEventBus

class ContainerViewController: UIViewController {
    
    var userId: Int?
    
    
    
    @IBOutlet weak var creditsLabel: UILabel!
    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var logoImageView: UIImageView!
    
    @IBOutlet weak var whiteContainer: UIImageView!
    @IBOutlet weak var creditsIcon: UIImageView!
    
    var backButtonNavigationController: UINavigationController?
    let faAttributes = [NSAttributedString.Key.font: UIFont.fontAwesome(ofSize: 20, style: .regular)] as Dictionary
    
    //si no esta activado, se muestra el menu
    var backButtonEnabled: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.reloadLogo()
        setFontAwesome()
        
        
       // self.navigationBar.isHidden = true
        
        //creating nav bar here
        
        self.navigationBar.isTranslucent = true
        self.navigationBar.setBackgroundImage(UIImage(), for: .default)
        
        self.view.backgroundColor = BColor.PrimaryBar
        enableBackButton(false,navController: nil)
        setNavigationTitle(nil)
        
        let fadeTextAnimation: CATransition = CATransition()
        fadeTextAnimation.duration = 0.5
        fadeTextAnimation.type = CATransitionType.fade
        self.navigationBar.layer.add(fadeTextAnimation, forKey: "fadeText")
      //  self.profileButton.tintColor = BColor.Light
        
        SwiftEventBus.onMainThread(self, name: Events.LOGO_CHANGED.rawValue) { result in
            self.reloadLogo()
        }
    }

    

    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let id = self.userId{
            UsersAPI.getUser(userId: id, { (serverUser: User?, error:  NSError?) in
                if let su = serverUser{
                    self.loadUser(su)
                }
            })
        }else{
            UsersAPI.getProfile { (serverUser: User?, error: NSError?) in
                if let su = serverUser{
                    self.loadUser(su)
                }
            }
        }
    }
    
    func loadUser(_ user: User){
        self.creditsLabel.text = "\(UIUtils.clean(user.credits))"
    }
    
    func reloadLogo(){
        UIUtils.loadLogo(logoImageView)
    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func leftMenuButtonPressed(_ sender: AnyObject) {
        if backButtonEnabled{
            let _ = self.backButtonNavigationController?.popViewController(animated: true)
            
            
        }else{
            self.slideMenuController()?.openLeft()
        }
        
    }
    
    func enableBackButton(_ bool: Bool, navController: UINavigationController?){
        if navController != nil{
            self.backButtonNavigationController = navController
        }
        
        if bool{
            //activar backButton
            //self.profileButton.title = String.fontAwesomeIconWithCode("fa-chevron-left")
          //  self.profileButton.image = UIImage(named: "back_arrow")
            self.backButtonEnabled = true
        }else{
            //self.profileButton.title = String.fontAwesomeIconWithCode("fa-bars")
           // self.profileButton.image = UIImage(named: "ic_bars")
            self.backButtonEnabled = false
        }
    }
    
    func setNavigationTitle(_ title: String?){
        if let t = title{
            hideImage(true,callback: {
                self.navigationBar.topItem?.title = t
            })
            
            
        }else{
            self.navigationBar.topItem?.title = nil
            hideImage(false, callback: nil)
            
        }
    }
    
    func setNavbarColor(_ color: UIColor?){
        if let c = color{
            changecolorAnimated(c)
        }else{
            changecolorAnimated(BColor.PrimaryBar)
            
        }
        
        
    }
    
    fileprivate func changecolorAnimated(_ color: UIColor){
        
        
        UIView.animate(withDuration: 0.5, animations: {() -> Void in
            
            
            self.view.backgroundColor = color
            }, completion: { _ in })

    }
    
    fileprivate func hideImage(_ hide: Bool, callback: (() -> Void)? ){
        
        if hide == false{
            self.logoImageView.isHidden = false
            self.creditsLabel.isHidden = false
            //self.creditsContainer.isHidden = false
        }
        
        UIView.animate(withDuration: 0.5, delay: 0, options: UIView.AnimationOptions(), animations: {() -> Void in
            // Animate the alpha value of your imageView from 1.0 to 0.0 here
            if hide == true{
                self.logoImageView.alpha = 0.0
                self.creditsLabel.alpha = 0.0
                self.whiteContainer.alpha = 0.0
                self.creditsIcon.alpha = 0.0
            }else{
                self.logoImageView.alpha = 1
                self.creditsLabel.alpha = 1
                self.whiteContainer.alpha = 1
                self.creditsIcon.alpha = 1
            }
            
            }, completion: {(finished: Bool) -> Void in
                // Once the animation is completed and the alpha has gone to 0.0, hide the view for good
                self.logoImageView.isHidden = hide
                self.creditsLabel.isHidden = hide
                self.whiteContainer.isHidden = hide
                self.creditsIcon.isHidden = hide
                callback?()
        })

    }
    
    
    fileprivate func setFontAwesome(){
       // profileButton.setTitleTextAttributes(faAttributes, for: UIControl.State.normal)
       // profileButton.setTitleTextAttributes(faAttributes, for: UIControl.State.highlighted)
       // profileButton.setTitleTextAttributes(faAttributes, for: UIControl.State.selected)
      //  profileButton.title = String.fontAwesomeIcon(name: .bars)
    }
    
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let id = segue.identifier
        if id == "ContainerViewEmbedSegue"{
            let destinationController = segue.destination as! TabBarController
            destinationController._enableBackButton = self.enableBackButton
            destinationController._setNavigationTitle = self.setNavigationTitle
            destinationController._setNavbarColor = self.setNavbarColor
            destinationController.loadViewIfNeeded()
        }
    }
    

}

