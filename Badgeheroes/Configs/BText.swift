//
//  Texts.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 6/2/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import Foundation

enum BFontSize: Int {
    case min = 8
    case normal = 12
    case max = 17
}
