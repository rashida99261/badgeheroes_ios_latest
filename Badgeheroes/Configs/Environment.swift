//
//  Environment.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 6/20/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import Foundation
import Firebase

open class Environment{
    
    
    static let FIREBASE_URL = "https://automated-skein-128822.firebaseio.com/"
    static let API_VERSION = 3
    
    #if DEBUG
        static let API_URL :String = "https://badgeheroes.herokuapp.com/api/"   //"http://localhost:3000/api/"
        static let STAGING_API_URL :String = API_URL
//        static let API_URL :String = "https://badgeheroes.herokuapp.com/api/"
//        static let API_URL :String = "https://badgeheroes-staging.herokuapp.com/api/"
        static let FCM_ENVIRONMENT: MessagingAPNSTokenType = MessagingAPNSTokenType.sandbox
    #else
        static let API_URL :String = "https://badgeheroes.herokuapp.com/api/"
        static let STAGING_API_URL :String = "https://badgeheroes-staging.herokuapp.com/api/"
        static let FCM_ENVIRONMENT: MessagingAPNSTokenType = MessagingAPNSTokenType.prod
    #endif
    
}
