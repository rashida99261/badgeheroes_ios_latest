//
//  LoginContactController.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 5/25/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import UIKit

class LoginContactController: BModalViewController , UITextViewDelegate{

    @IBOutlet weak var companyName: UITextView!
    @IBOutlet weak var comments: UITextView!
    @IBOutlet weak var spaceTblVw: UITableView!
    
    var email: String = ""
    var rowCount : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.clear
        view.isOpaque = false
        
        companyName.text = "Nombre de tu empresa"
        companyName.textColor = UIColor.lightGray
        
        comments.text = "Escribe un mensaje"
        comments.textColor = UIColor.lightGray
    }
    
    override func viewDidAppear(_ animated: Bool) {
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.enableNavbar?(false)
    }
    
    @IBAction func clickOnBackBtn(_ sender : UIButton){
        
        dismiss(animated: true, completion: {
            let dict = ["dismissfrom": ""]
            NotificationCenter.default.post(name: Notification.Name("isDismissViewController"), object: nil, userInfo: dict)
        })
    }
    
    @IBAction func acceptButtonPressed(_ sender: AnyObject) {
        let companyName = self.companyName.text!
        let comments = self.comments.text!
        UsersAPI.userNotFound(email: email, companyName: companyName, comments: comments) { (user: User?, error: NSError?) in
            if error != nil{
                
                UIUtils.toast("No se ha podido comunicar con el servidor")
                if let callback = self.modalFinished{
                    callback()
                }
            }else{
                
                UIUtils.toast("Se han enviado tus comentarios al administrador.")
                if let callback = self.modalFinished{
                    callback()
                }
                //navigate back to login
                self.dismiss(animated: true, completion: {
                    let dict:[String: String] = ["dismissfrom": "callback"]
                    NotificationCenter.default.post(name: Notification.Name("isDismissViewController"), object: nil, userInfo: dict)
                    
                })
            }
        }
    }
    
    
    //delegate methods
    func textViewDidBeginEditing(_ textView: UITextView) {
        if(textView == companyName || textView == comments){
            if textView.textColor == UIColor.lightGray {
                textView.text = ""
                textView.textColor = UIColor.black
                rowCount = 1
                spaceTblVw.reloadData()
            }
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if(textView == companyName){
            if textView.text.isEmpty {
                textView.text = "Nombre de tu empresa"
                textView.textColor = UIColor.lightGray
            }
        }
        
        if(textView == comments){
            if textView.text.isEmpty {
                textView.text = "Escribe un mensaje"
                textView.textColor = UIColor.lightGray
            }
        }
        rowCount = 0
        spaceTblVw.reloadData()
    }
}

extension LoginContactController : UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rowCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : UITableViewCell = spaceTblVw.dequeueReusableCell(withIdentifier: "spacecell")!
        return cell
    }
}
