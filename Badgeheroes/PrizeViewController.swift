//
//  PrizeViewController.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 7/19/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import UIKit
import JDSlider
import SwiftEventBus

class PrizeViewController: BaseUIViewController , UIGestureRecognizerDelegate{
    
  //  @IBOutlet weak var carouselView: JDSliderView!
    @IBOutlet weak var carouselViewImg: UIImageView!
    @IBOutlet weak var credits: UILabel!
    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var navBarView: UIView!

    @IBOutlet weak var descriptionLabel: UITextView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var creditView: UIView!
    @IBOutlet weak var topCreditLbl: UILabel!
    //@IBOutlet weak var lblStock: UILabel!
    @IBOutlet weak var logoImageView: UIImageView!
    
    @IBOutlet weak var btnCanjear : UIButton!
    @IBOutlet weak var imgDollar : UIImageView!
    @IBOutlet weak var lblBottomView : UILabel!
    
    @IBOutlet weak var viewFailure: UIView!
    @IBOutlet weak var btnCross: UIButton!
    @IBOutlet weak var btnIRC: UIButton!
    @IBOutlet weak var bottomViewHeight: NSLayoutConstraint!
    
    
    var prize: Prize!
   // var images: [UIImageView] = [UIImageView]()
    var success = false
    var topCredit : String = ""
    
    var imageArray : [String] = []
    
    var Pageint : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.reloadLogo()
        
        self.navBarView.backgroundColor = BColor.PrimaryBar
        self.viewFailure.isHidden = true
        self.btnIRC.backgroundColor = BColor.PrimaryBar
        
     //   self.carouselView.pageControl.backgroundColor = UIColor.clear
     //   self.carouselView.pageControl.isUserInteractionEnabled = false
       // self.carouselView.backgroundColor = BColor.TabBackground
        //        self.loader.startAnimating()
        
        
        carouselViewImg.isUserInteractionEnabled = true
        
        let leftSwipeGesture = UISwipeGestureRecognizer.init(target: self, action: #selector(swipeLeftToRight(gesture:)))
        leftSwipeGesture.delegate = self
        leftSwipeGesture.direction = .left
        carouselViewImg.addGestureRecognizer(leftSwipeGesture)
        
        let rightSwipeGesture = UISwipeGestureRecognizer.init(target: self, action: #selector(swipeLeftToRight(gesture:)))
        rightSwipeGesture.delegate = self
        rightSwipeGesture.direction = .right
        carouselViewImg.addGestureRecognizer(rightSwipeGesture)
        
        topCreditLbl.text = topCredit
        topBarCredit = topCreditLbl.text!
        
        
        SwiftEventBus.onMainThread(self, name: Events.LOGO_CHANGED.rawValue) { result in
            self.reloadLogo()
        }
        
        PrizesAPI.getPrize(prize.id!) { (prize, error) in
        //    self.carouselView.reloadData()
            self.prize = prize
            
            if let prize = self.prize{
                
                self.credits.text = "\(UIUtils.clean(prize.credits))"
                self.amount.text = "Quedan \(UIUtils.clean(prize.amount)) unidades"
                
                if(Int(self.topCredit)! < Int(prize.credits!)){
                    let ccAmt = Int(prize.credits!) - Int(self.topCredit)!
                    
                    self.lblBottomView.isHidden = false
                    self.lblBottomView.text = "Te faltan $\(ccAmt) para canjear este producto"
                    self.lblBottomView.textColor = BColor.PrimaryBar
                    
                    self.btnCanjear.setTitle("CONSEGUIR MAS MONEDAS", for: UIControl.State.normal)
                    self.btnCanjear.backgroundColor = BColor.PrimaryBar
                    self.credits.textColor = UIColor.init(red: 0.7, green: 0.7, blue: 0.7, alpha: 1)
                    
                    self.bottomViewHeight.constant = 120
                    
                    guard let currentCGImage = UIImage(named: "top_bar_credits_icon")!.cgImage else { return }
                    let currentCIImage = CIImage(cgImage: currentCGImage)
                    
                    let filter = CIFilter(name: "CIColorMonochrome")
                    filter?.setValue(currentCIImage, forKey: "inputImage")
                    
                    // set a gray value for the tint color
                    filter?.setValue(CIColor(red: 0.7, green: 0.7, blue: 0.7), forKey: "inputColor")
                    
                    filter?.setValue(1.0, forKey: "inputIntensity")
                    guard let outputImage = filter?.outputImage else { return }
                    
                    let context = CIContext()
                    
                    if let cgimg = context.createCGImage(outputImage, from: outputImage.extent) {
                        let processedImage = UIImage(cgImage: cgimg)
                        self.imgDollar.image = processedImage
                    }
                }
                else{
                    self.btnCanjear.setTitle("CANJEAR POR $\(prize.credits!)", for: UIControl.State.normal)
                    self.lblBottomView.isHidden = true
                    self.imgDollar.image = UIImage(named: "top_bar_credits_icon")
                    self.bottomViewHeight.constant = 95
                }
                
                
                let strDespTitle =  [NSAttributedString.Key.font: UIFont(name: "Arial-Black", size: 15)!,NSAttributedString.Key.foregroundColor: self.UIColorFromHex(rgbValue: 0x969898, alpha: 1.0)]
                let myString = NSMutableAttributedString(string: "Discripción: ", attributes: strDespTitle)
                
                let strDescription =  [NSAttributedString.Key.font: UIFont(name: "TitilliumWeb-SemiBold", size: 15)!,NSAttributedString.Key.foregroundColor: self.UIColorFromHex(rgbValue: 0x969898, alpha: 1.0)]
                let strText = NSAttributedString(string: "\(UIUtils.clean(prize.description))", attributes: strDescription)
                myString.append(strText)
                self.descriptionLabel.attributedText = myString
                self.titleLbl.text = UIUtils.clean(prize.name)
                
                var downloaded = 0
                if let iurls = prize.imageUrls {
                    for url in iurls{
                       // let imageView = UIImageView()
                      //  frame: CGRect(x: self.carouselView.frame.width - 20 / 2, y: 0, width: self.carouselView.frame.width - 20, height: self.carouselView.frame.height)
                        
                      //  imageView.contentMode = UIView.ContentMode.scaleAspectFit
                        self.imageArray.append(url)
                    }
                    
                    self.carouselViewImg.kf.setImage(with: URL(string: self.imageArray[0])!, placeholder: nil, options: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, imageURL) in
                    })
                    self.Pageint = 0
                    self.loader.stopAnimating()
                }
            }
        }
    }
    
    
    @objc func swipeLeftToRight(gesture : UISwipeGestureRecognizer)
    {
        if (gesture.direction == .left) {
            if (Pageint < self.imageArray.count - 1) {
                carouselViewImg.slideInFromRight()
                //pageControl.currentPage = Pageint + 1
                Pageint += 1
                
                self.carouselViewImg.kf.setImage(with: URL(string: imageArray[Pageint])!, placeholder: nil, options: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, imageURL) in
                })
            }
        }
        if (gesture.direction == .right) {
            if (Pageint > 0) {
                carouselViewImg.slideInFromLeft()
                //pageControl.currentPage = Pageint + 1
                Pageint -= 1
                
                self.carouselViewImg.kf.setImage(with: URL(string: imageArray[Pageint])!, placeholder: nil, options: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, imageURL) in
                })
            }
        }
    }
        
    @IBAction func redeemButtonPressed(_ sender: AnyObject) {
        PrizesAPI.redeemPrize(self.prize.id!) { (response, error) in
            if let r = response{
                if let s = r.status , s == "ok"{
                    self.success = true
                    self.performSegue(withIdentifier: "PrizeToRedeemFinished", sender: self)
                    return
                }
            }
            self.success = false
            self.viewFailure.isHidden = false
            //self.performSegue(withIdentifier: "PrizeToRedeemFinished", sender: self)
            return
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let tb = self.tabBarController as? TabBarController{
            tb.setNavigationTitle(prize.name)
            tb.setNavbarColor(nil)
            tb.enableBackButton(true, navController: self.navigationController)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    @IBAction func clickOnCross(_ sender: UIButton){
        self.viewFailure.isHidden = true
    }
    
    @IBAction func clickOnIR(_ sender: UIButton){
        self.tabBarController?.selectedIndex = 0
        self.navigationController?.popViewController(animated: false)
        
    }


//    //MARK: JDSliderDelegate
//    func slider(_ jdSlider: JDSliderView, didSelectSlideAtIndex index: Int) {
//        print("Touch slide with index: \(index)")
//    }
//    
//    //MARK: JDSliderDataSource
//    func slider(jdSliderNumberOfSlides slider: JDSliderView) -> Int {
//        
//        if let images =  self.prize.imageUrls?.count{
//            return images
//        }else{
//            return 0
//        }
//    }
    
//    func slider(_ jdSlider: JDSliderView, viewForSlideAtIndex index: Int) -> UIView {
//        //let url = self.prize.imageUrls![index]
//
//        if index < self.images.count{
//
//            print(images[index].frame)
//
//            return images[index]
//        }else{
//            return UIImageView()
//        }
//    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "PrizeToRedeemFinished"{
            let controller = segue.destination as! RedeemFinishedViewController
            controller.success = self.success
        }
    }
    
    @IBAction func clickOnBackBtn(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    func reloadLogo(){
        UIUtils.loadLogo(logoImageView)
    }
   
}
extension UIView {
    // Name this function in a way that makes sense to you...
    // slideFromLeft, slideRight, slideLeftToRight, etc. are great alternative names
    func slideInFromLeft(duration: TimeInterval = 1, completionDelegate: AnyObject? = nil) {
        // Create a CATransition animation
        let slideInFromLeftTransition = CATransition()
        
        // Set its callback delegate to the completionDelegate that was provided (if any)
        if let delegate: AnyObject = completionDelegate {
            slideInFromLeftTransition.delegate = delegate as? CAAnimationDelegate
        }
        
        // Customize the animation's properties
        slideInFromLeftTransition.type = CATransitionType.push
        slideInFromLeftTransition.subtype = CATransitionSubtype.fromLeft
        slideInFromLeftTransition.duration = duration
        slideInFromLeftTransition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        slideInFromLeftTransition.fillMode = CAMediaTimingFillMode.removed
        
        // Add the animation to the View's layer
        self.layer.add(slideInFromLeftTransition, forKey: "slideInFromLeftTransition")
    }
    
    func slideInFromRight(duration: TimeInterval = 1, completionDelegate: AnyObject? = nil) {
        // Create a CATransition animation
        let slideInFromLeftTransition = CATransition()
        
        // Set its callback delegate to the completionDelegate that was provided (if any)
        if let delegate: AnyObject = completionDelegate {
            slideInFromLeftTransition.delegate = delegate as? CAAnimationDelegate
        }
        
        // Customize the animation's properties
        slideInFromLeftTransition.type = CATransitionType.push
        slideInFromLeftTransition.subtype = CATransitionSubtype.fromRight
        slideInFromLeftTransition.duration = duration
        slideInFromLeftTransition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        slideInFromLeftTransition.fillMode = CAMediaTimingFillMode.removed
        
        // Add the animation to the View's layer
        self.layer.add(slideInFromLeftTransition, forKey: "slideInFromLeftTransition")
    }
}

extension UIViewController{
    func UIColorFromHex(rgbValue:UInt32, alpha:Double=1.0)->UIColor {
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        return UIColor(red:red, green:green, blue:blue, alpha:CGFloat(alpha))
    }
}
