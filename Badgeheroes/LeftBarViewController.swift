//
//  LeftBarViewController.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 5/24/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import UIKit


class LeftBarViewController: UIViewController {
    

    @IBOutlet weak var userImage: UIImageView!
    
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userEmail: UILabel!
    
    @IBOutlet weak var profileButton: UIButton!
    
    @IBOutlet weak var contactButton: UIButton!
    
    @IBOutlet weak var logoutButton: UIButton!
    //@IBOutlet weak var helpButton: UIButton!
    
    @IBOutlet weak var myPrizesButton: UIButton!
    
    @IBOutlet weak var userImageBackground: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIUtils.toCircleImage(self.userImage)
        
        self.view.frame = CGRect(x: self.view.frame.minX, y: self.view.frame.minY, width: UIScreen.main.bounds.width*0.8 , height: self.view.frame.height )
        setFontAwesome()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.userName.text = Settings.FullName
        self.userEmail.text = Settings.Email
        if let url = URL(string: Settings.ImageUrl ){
            self.userImage.kf.setImage(with: url)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.userImageBackground.backgroundColor = BColor.SecondaryBar
    }
    
    @IBAction func logoutButtonPressed(_ sender: AnyObject) {
        Settings.logout()
        BColor.toDefaultColors()
        GlobalBgColor = nil
        
        isTapOnRow = ""
        isTapOnRowRadioBtn = ""
        
        let emailController = self.storyboard!.instantiateViewController(withIdentifier: "LoginContainerViewController") as! LoginContainerViewController
        emailController.modalFinished = {
            self.dismiss(animated: true, completion: nil)
        }
        
        closeLeft()
        UIUtils.showInDefaultModal(self, controller: emailController, headerBarEnabled: false)
    }
    
    @IBAction func profileButtonPressed(_ sender: AnyObject) {
        print("Settings.Email")
        print(Settings.Email)
        if Settings.Email != "demo@badgeheroes.com"{
            let editController = self.storyboard!.instantiateViewController(withIdentifier: "EditProfileViewController") as! EditProfileViewController
            
            closeLeft()
            self.present(editController, animated: true, completion: nil)
        } else{
            UIUtils.toast("Debes tener tu propia cuenta para poder realizar esta acción")
        }
    }
    
    @IBAction func contactButtonPressed(_ sender: AnyObject) {
       // let contactController = self.storyboard!.instantiateViewController(withIdentifier: "GeneralContactTypeSelectorViewController") as! GeneralContactTypeSelectorViewController
        
        let contactController = self.storyboard!.instantiateViewController(withIdentifier: "GeneralContactViewController") as! GeneralContactViewController
        closeLeft()
         //isOpenFrstTym = "true"
       // contactController.modalFinished = self.modalFinished
        //contactController.commentType = 0
        self.present(contactController, animated: true, completion: nil)
        
       //self.navigationController?.pushViewController(contactController, animated: true)
        //UIUtils.showInDefaultModal(self, controller: contactController)
    }
    
    
    @IBAction func myPrizesButtonPressed(_ sender: AnyObject) {
        let myPrizesController = self.storyboard!.instantiateViewController(withIdentifier: "MyPrizesViewController") as! MyPrizesViewController
        closeLeft()
        self.present(myPrizesController, animated: true, completion: nil)
        //UIUtils.showInDefaultModal(self, controller: myPrizesController)
    }
    
    @IBAction func helpButtonPressed(_ sender: AnyObject) {
    }
    
    @IBAction func backButtonPressed(_ sender: AnyObject) {
        closeLeft()
    }
    
    func setFontAwesome(){
        let attributes = UIFont.fontAwesome(ofSize: 20, style: .regular)
        
        profileButton.titleLabel?.font = attributes
        logoutButton.titleLabel?.font = attributes
        contactButton.titleLabel?.font = attributes
       // helpButton.titleLabel?.font = attributes
        
        addBottomLayerWithColor(passBtn: profileButton)
        addBottomLayerWithColor(passBtn: logoutButton)
        addBottomLayerWithColor(passBtn: contactButton)
       // addBottomLayerWithColor(passBtn: helpButton)
        addBottomLayerWithColor(passBtn: myPrizesButton)
        
//        profileButton.setTitle(String.fontAwesomeIcon(name: .user), for: UIControl.State())
//        logoutButton.setTitle(String.fontAwesomeIcon(name: .powerOff), for: UIControl.State())
//        contactButton.setTitle(String.fontAwesomeIcon(name: .envelope), for: UIControl.State())
    }
    
    func addBottomLayerWithColor(passBtn : UIButton)
    {
        let border = CALayer()
        border.backgroundColor = UIColor.black.cgColor
        border.frame = CGRect(x:20, y:passBtn.frame.height, width:self.view.frame.width-50, height:0.3)
        passBtn.layer.addSublayer(border)
    }
}



