//
//  BContainerView.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 6/3/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import UIKit

class BCollectionView: UICollectionView {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //self.backgroundColor = BColor.TabBackground
    }
}

class BTableView: UITableView {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //self.backgroundColor = BColor.TabBackground
    }
}
