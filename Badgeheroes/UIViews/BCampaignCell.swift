//
//  BCampaignCell.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 5/24/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import UIKit
import Kingfisher

var isBonusPresent = ""

class BCampaignCell: UICollectionViewCell {
    
    @IBOutlet weak var campaignView: BCampaignView!
    @IBOutlet weak var campaignImage: UIImageView!
    @IBOutlet weak var campaignName: UILabel!
    @IBOutlet weak var progressLabel: UILabel!
    @IBOutlet weak var progressView: UIView!
   // @IBOutlet weak var remainingTimeView: UIView!
    @IBOutlet weak var imgstarBonus: UIImageView!
    @IBOutlet weak var remainingTimeLabel: UILabel!
    @IBOutlet weak var stateIcon: UILabel!
    @IBOutlet weak var checkIcon: UIImageView!
    @IBOutlet weak var progressBarStatus: UIProgressView!
    
    var categoryId: Int!
    var isLocked: Bool = false
    var imageUrl: URL!
    var endDate: Date?
    var campaign: Campaign?
    var timer: Timer?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //self.backgroundColor = UIColor.clear
    }
    
    func setCampaign(campaign: Campaign){
        self.campaign = campaign
        
        if campaign.HierarchyType == Campaign.CampaignHierarchyType.bonus && self.campaign?.State != .completed{
            isBonusPresent = "true"
            self.remainingTimeLabel.isHidden = false
            
            
            var strRemanTime = campaign.RemainingTimeMessage.components(separatedBy: " ")
            self.remainingTimeLabel.text = "\(strRemanTime[0])"
            self.progressLabel.isHidden = true
        }else{
           // isBonusPresent = ""
            self.remainingTimeLabel.isHidden = true
            self.progressLabel.isHidden = false
            self.progressLabel.text = "\(campaign.completedMissions!)/\(campaign.totalMissions!)"
        }
        
        self.campaignName.text = campaign.name
        self.stateIcon.isHidden = true
        
        self.setProgressbar(campaign: campaign)
        
        imageUrl = URL(string: campaign.imageUrl! )!
        endDate = campaign.endDate
        
        
    
        self.campaignView.backgroundColor = UIColor.clear
        self.reloadRemainingTime()
       // UIUtils.toCircleView(self.remainingTimeView, cornerRadius: 0.1)
        
    }
    
    func setProgressbar(campaign: Campaign) {
        self.progressBarStatus.progress = Float(campaign.completedMissions!) / Float(campaign.totalMissions!)
        self.progressBarStatus.progressTintColor = campaign.Color
        self.progressBarStatus.layer.cornerRadius = 2
        self.progressBarStatus.layer.masksToBounds = true
        
        if let _ = self.progressView
        {
            self.progressView.layer.cornerRadius = 12
            self.progressView.layer.masksToBounds = true
        }
    }
    
    func setRemainingTime(){
        if self.campaign?.HierarchyType == Campaign.CampaignHierarchyType.bonus && self.campaign?.State != .completed{
            //self.remainingTimeView.isHidden = false
            self.imgstarBonus.isHidden = false
            isBonusPresent = "true"
            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(BCampaignCell.reloadRemainingTime), userInfo: nil, repeats: true)
        }else{
           // self.remainingTimeView.isHidden = true
            self.imgstarBonus.isHidden = true
        }
    }
    
    @objc func reloadRemainingTime(){
        if let campaign = self.campaign{
            var strRemanTime = campaign.RemainingTimeMessage.components(separatedBy: " ")
            self.remainingTimeLabel.text = "\(strRemanTime[0])"
        }
    }
    
    func cellPressed(){
        if self.isLocked == false{
            self.backgroundColor = UIColor.clear
        }
    }
    
    func cellReleased(){
        if self.isLocked == false{
            self.backgroundColor = UIColor.clear
        }
    }
    
    func toAvailable(){
        setImage(type: ImageType.normal)
        self.checkIcon.isHidden = true
        if let _ = self.progressView
        {
            self.progressView.isHidden = false
        }
        setFontAwesome("")
        self.campaignName.textColor = UIColor(hexString: "#646464")
        self.progressLabel.textColor = UIColor(hexString: "#646464")
        self.remainingTimeLabel.textColor = UIColor(hexString: "#646464")
        setRemainingTime()
    }
    
    func toLocked(){
        self.isLocked = true
        self.checkIcon.isHidden = true
        setFontAwesome("")
        
        self.progressView.isHidden = true
        self.progressLabel.textColor = UIColor.gray
        self.remainingTimeLabel.textColor = UIColor.gray
        setImage(type: ImageType.gray)
        self.campaignName.textColor = UIColor.gray
        setRemainingTime()
    }
    
    func toExpired(){
        self.isLocked = true
        //self.backgroundColor = BColor.UnlockedCellBackground
        //self.stateIcon.isHidden = false
        self.checkIcon.isHidden = true
        self.progressView.isHidden = true
        
        setFontAwesome("fa-lock")
        setImage(type: ImageType.gray)
        
        self.progressLabel.textColor = UIColor.gray
        self.remainingTimeLabel.textColor = UIColor.gray
        self.campaignName.textColor = UIColor.gray
        setRemainingTime()
    }
    
    func toAccepted(){
        self.isLocked = true
        self.checkIcon.isHidden = false
        setImage(type: ImageType.normal)
        
        self.progressLabel.textColor = UIColor(hexString: "#646464")
        self.remainingTimeLabel.textColor = UIColor(hexString: "#646464")
        self.campaignName.textColor = UIColor(hexString: "#646464")
        
        if let _ = self.progressView
        {
            self.progressView.isHidden = true
        }
        setRemainingTime()
    }
    
    fileprivate func setFontAwesome(_ code: String){
        self.stateIcon.font = UIFont.fontAwesome(ofSize: 20, style: .regular)
        self.stateIcon.textColor = UIColor.gray
        self.stateIcon.text = String.fontAwesome(code: code).map { $0.rawValue }
    }
    
    func setImage(type: ImageType) {
        
        switch type {
        case .normal:
            self.campaignImage.kf.setImage(with: self.imageUrl, placeholder: nil, options: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, url) in
                    self.campaignImage.isHidden = false
            })
        case .gray:
            KingfisherManager.shared.retrieveImage(with: self.imageUrl, options: nil, progressBlock: nil) { (image, error, cacheType, url) in
                if let i = image {
                    let grayImage = UIUtils.convertToGrayScale(image: i)
                    self.campaignImage.image = grayImage
                    self.campaignImage.isHidden = false
                } else {
                    self.campaignImage.image = nil
                }
            }
            
        case .gold:
            KingfisherManager.shared.retrieveImage(with: self.imageUrl, options: nil, progressBlock: nil) { (image, error, cacheType, url) in
                if let i = image{
                    let grayImage = UIUtils.convertToGoldenScale(image: i)
                    self.campaignImage.image = grayImage
                    self.campaignImage.isHidden = false
                }else{
                    self.campaignImage.image = nil
                }
            }
        }
        
    }
    
    enum ImageType : Int {
        case normal = 0
        case gray = 1
        case gold = 2
    }
    
}
