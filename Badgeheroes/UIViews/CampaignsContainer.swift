//
//  CampaignsContainer.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 6/8/17.
//  Copyright © 2017 MisPistachos. All rights reserved.
//

import UIKit

class CampaignContainer {
    
    var campaigns: [Campaign]
    var rowMode = false
    var row = 0
    var fullMode = false
    var width: CGFloat = 0
    
    init(campaigns: [Campaign], row: Int, screenWidth: CGFloat, withHierarchy: Bool, requiredCampaignsCompletedInPreviousRow: Bool)
    {
        self.campaigns = campaigns.sorted(by: { $0.Col < $1.Col })
        self.row = row
        self.width = screenWidth
        for campaign in campaigns
        {
            if (withHierarchy)
            {
                if(requiredCampaignsCompletedInPreviousRow){
                    campaign.internalState = Campaign.CampaignState.available
                }else{
                    campaign.internalState = Campaign.CampaignState.locked
                }
                
            }else{
                campaign.internalState = Campaign.CampaignState.available
            }
        }
    }
    
    func campaignWidth() -> CGFloat{
        if self.fullMode{
            //campaignStr = "single"
            return self.width
        }else{
            //campaignStr = "multiple"
            return self.width/CGFloat(self.campaigns.count) - 1
        }
    }
    
    
    
    
    
    func campaignHeight() -> CGFloat{
        if self.fullMode{
            return (width / 2) - 50
        }else{
            return (width * 1 / 2.0) - 15
        }
    }
    
    static func load(campaigns: [Campaign], campaignTree: CampaignTree, screenWidth: CGFloat) -> [CampaignContainer]{
        
        let groups = campaigns.group { (campaign: Campaign) -> Int in
            
            return campaign.Row
        }
        var campaignContainers: [CampaignContainer] = []
        
        let sortedKeys = Array(groups.keys).sorted(by: { $0 < $1 })
        
        var requiredCampaignsCompletedInPreviousRow = true
        for key in sortedKeys {
            let campaigns = groups[key]
            
            let campaignContainer = CampaignContainer(campaigns: campaigns!, row: key, screenWidth: screenWidth, withHierarchy: campaignTree.Hierarchy, requiredCampaignsCompletedInPreviousRow: requiredCampaignsCompletedInPreviousRow)
            
            if campaigns?.count == 1 && (campaigns![0].FullMode){
                campaignContainer.fullMode = true
            }
            
            //Update requiredCampaignsCompletedInPreviousRow
            if requiredCampaignsCompletedInPreviousRow == true{
                for campaign in campaignContainer.campaigns{
                    if campaign.HierarchyType == .required && !campaign.Completed{
                        requiredCampaignsCompletedInPreviousRow = false
                        break;
                    }
                    
                }
            }
            campaignContainers.append(campaignContainer)
        }
        return campaignContainers
        
        
    }
    
}
