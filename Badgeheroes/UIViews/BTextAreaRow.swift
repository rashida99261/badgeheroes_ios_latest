//
//  BTextAreaRow.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 6/24/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import UIKit

class BTextAreaRow: BBaseRow{
   
    @IBOutlet weak var textView: UITextView!
    
    override func completePage() {
        let ans = self.textView.text
        if ans != "" && ans != "Escribe tu respuesta aquí"{
            if self.pageElement != nil{
                self.pageElement.answerText = textView.text
                self.pageElement.isAnswered = true
            }
        }
    }
    
    override func loadDefault(){
        if let ans = pageElement.answerText{
            self.textView.text = ans
        }
    }
}
