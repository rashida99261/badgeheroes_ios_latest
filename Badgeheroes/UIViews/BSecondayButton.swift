//
//  BSecondayButton.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 6/6/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import UIKit

class BSecondaryButton: UIButton {
    
    var isButtonPressed = false
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
//        self.backgroundColor = BColor.ComplementButtonNormal
//        self.setTitleColor(BColor.ComplementButtonNormalText, for: UIControlState())
       // self.backgroundColor = UIColor(hexString: "#FAB40A")
        //self.setTitleColor(UIColor(hexString: "#FFFFFF"), for: .normal)
        
        self.layer.cornerRadius = 25
        self.layer.masksToBounds = true
        
        self.addTarget(self, action: #selector(BSecondaryButton.buttonReleased), for: UIControl.Event.touchUpInside)
        self.addTarget(self, action: #selector(BSecondaryButton.buttonPressed), for: UIControl.Event.touchDown)
        
    
    }
    
    @objc func buttonPressed(){
        isButtonPressed = true
        
        //self.backgroundColor = BColor.ComplementButtonPressed
    }
    
    @objc func buttonReleased(){
        
        if isButtonPressed{
          //  self.backgroundColor = BColor.ComplementButtonNormal
            isButtonPressed = false
            lockButton()
        }
        
    }
    
    func lockButton(){
       // self.backgroundColor = BColor.ComplementButtonPressed
        self.isEnabled = false
    }
    
    func unlockButton(){
        //self.backgroundColor = BColor.ComplementButtonNormal
        self.isEnabled = true
    }
}
