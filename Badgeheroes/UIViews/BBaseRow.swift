//
//  File.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 6/29/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import UIKit

class BBaseRow: UITableViewCell{
    
    var pageElement: PageElement!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //self.backgroundColor = BColor.TabBackground
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?){
        super.init(style: style, reuseIdentifier: reuseIdentifier)
       // self.backgroundColor = BColor.TabBackground
    }
    
    
    func completePage(){
        if self.pageElement != nil{
            self.pageElement.isAnswered = true
        }
        
    }
    
    func loadDefault(){
        
    }

}
