//
//  TextFieldController.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 5/23/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import UIKit

class BUITextField: UITextField, UITextFieldDelegate {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.delegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.delegate = self
        self.layer.cornerRadius = 22
        self.layer.masksToBounds = true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
