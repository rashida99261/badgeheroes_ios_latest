//
//  BFormButton.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 6/1/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import UIKit

class BPrimaryButton: UIButton {
    
    var isButtonPressed = false
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
       // self.backgroundColor = BColor.ButtonPrimary
        
        self.layer.cornerRadius = 25
        self.layer.masksToBounds = true
        
      //  self.setTitleColor(BColor.ButtonNormalText, for: UIControl.State())
        self.addTarget(self, action: #selector(BPrimaryButton.buttonReleased), for: UIControl.Event.touchUpInside)
        self.addTarget(self, action: #selector(BPrimaryButton.buttonPressed), for: UIControl.Event.touchDown)
    }
    
    @objc func buttonPressed(){
      //  isButtonPressed = true
       // self.backgroundColor = BColor.ButtonPressed
    }
    
    @objc func buttonReleased(){
        
        if isButtonPressed {
          //  self.backgroundColor = BColor.ButtonNormal
            isButtonPressed = false
            lockButton()
        }
    }
    
    func lockButton(){
       // self.backgroundColor = BColor.ButtonPressed
        self.isEnabled = false
    }
    
    func unlockButton(){
        //self.backgroundColor = BColor.ButtonNormal
        self.isEnabled = true
    }
    
    
}
