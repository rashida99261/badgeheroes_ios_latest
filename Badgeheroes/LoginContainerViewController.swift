//
//  LoginContainerViewController.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 7/22/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import UIKit

class LoginContainerViewController: BModalViewController {
    @IBOutlet weak var navBar: BNavigationBar!
    
    @IBOutlet var generalView: BTabView!
    
    @IBOutlet weak var backButton: UIBarButtonItem!
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var navbarHeight: NSLayoutConstraint!
    
    @IBOutlet weak var navbarTop: NSLayoutConstraint!
    var embeddedController: UIViewController!
    
    var backButtonFunction: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setFontAwesome()
        
        
        self.generalView.backgroundColor = BColor.PrimaryBar
        _enableNavbar(true)
    }
    
    @IBAction func backButtonPressed(_ sender: AnyObject) {
        
        if let backFunction = backButtonFunction{
            backFunction()
        }
       
    }
    
    func _enableNavbar(_ boolean: Bool){
        if boolean{
            self.navbarHeight.constant = 44
            self.navbarTop.constant = 60
            logoImageView.isHidden = false
        }else{
            self.navbarHeight.constant = 0
            self.navbarTop.constant = 0
            logoImageView.isHidden = true
        }
        
        
        self.navBar.layoutIfNeeded()
        self.navBar.layoutMarginsDidChange()
        self.navBar.superview?.layoutIfNeeded()
        self.view.layoutIfNeeded()
        self.view.layoutMarginsDidChange()
        self.view.layoutIfNeeded()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "LoginContainerSegue"{
            let destinationController = segue.destination as! LoginNavigationController
            self.backButtonFunction = destinationController.backButtonPressed
            destinationController.enableNavbar = self._enableNavbar
            destinationController.modalFinished = self.modalFinished
        }
        
    }
    
 
    
    func setFontAwesome(){
        let attributes = [NSAttributedString.Key.font: UIFont.fontAwesome(ofSize: 20, style: .regular)] as Dictionary
        
        backButton.setTitleTextAttributes(attributes, for: UIControl.State())
        backButton.title = String.fontAwesomeIcon(name: .chevronLeft)
    }
}
