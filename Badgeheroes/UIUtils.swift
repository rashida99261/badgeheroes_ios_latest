//
//  UIUtils.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 5/25/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import UIKit
import Foundation
import Toaster
import SwiftEventBus
import NVActivityIndicatorView

open class UIUtils{
    
    static func setApplicationUser(u: User?, mf: (() -> Void)?) -> Bool{
        if let user = u{
            if user.Valid {
                Settings.AccessToken = user.accessToken!
                Settings.FirebaseAccessToken = user.firebaseAccessToken!
                Settings.FirstName = user.firstName!
                Settings.LastName = user.lastName!
                Settings.UserId = user.id!
                Settings.IsStagingMode = user.stagingMode!
                Settings.Email = user.email!
                Settings.ImageUrl = user.imageUrl!
                
                if let tid = user.teamId{
                    Settings.TeamId = tid
                }
                
                if let cid = user.clientId{
                    Settings.ClientId = cid
                }
                
                if let modalFinished = mf {
                    
                    UIUtils.loadCustomColorsAndImage(loadedFunction: {
                        modalFinished()
                    })
                }
                return true
            }
            else{
              //
               // print("ddddddddddddddddddd")
            }
        }
        return false
    }
    
    public static func addLoader(_ view: UIView, y: Int?) -> NVActivityIndicatorView{
        let width = 70
        var y_ = 100
        if y != nil{
            y_ = y!
        }
        let rect = CGRect(x: Int(UIScreen.main.bounds.midX) - Int(width/2), y: y_, width: width, height: width)
        let loader: NVActivityIndicatorView = NVActivityIndicatorView(frame: rect, type: NVActivityIndicatorType.ballSpinFadeLoader, color: BColor.LoaderColor, padding: nil)
        
        view.addSubview(loader)
        
        return loader
    }
    
    public static func toCircleImage(_ image: UIImageView){
        image.layoutIfNeeded()
        image.layer.cornerRadius =  image.frame.size.width / 2
        image.clipsToBounds = true
    }
    
    public static func toCircleImage(_ image: UIButton){
        image.layoutIfNeeded()
        image.layer.cornerRadius = image.frame.size.width / 2
        image.clipsToBounds = true
    }
    
    public static func toCircleView(_ view: UIView, cornerRadius: CGFloat){
        view.layoutIfNeeded()
        view.layer.cornerRadius = view.frame.size.width*cornerRadius
        view.clipsToBounds = true
    }
    
    public static func toast(_ message: String){
        let toast = Toast(text: message)
        toast.start()
        
    }
    
    public static func loadLogo(_ imageView: UIImageView ){
        if Settings.Logo != ""{
            let path = Settings.Logo
            imageView.kf.setImage(with: URL(string: path)!)

        }
        
    }
    
    
    fileprivate static func attributedStringWithFontSize(_ nsa: NSAttributedString ,fontSize: CGFloat) -> NSAttributedString {
        let attributedString: NSMutableAttributedString = nsa.mutableCopy() as! NSMutableAttributedString
        
        attributedString.beginEditing()
        attributedString.enumerateAttribute(NSAttributedString.Key.font, in: NSMakeRange(0, attributedString.length), options: NSAttributedString.EnumerationOptions.reverse, using: {(value: Any?, range: NSRange, _) -> Void in
            let font: UIFont = value! as! UIFont
            
            font.withSize(fontSize)
            let lowerFont = font.fontName.lowercased()//.stringByReplacingOccurrencesOfString("TimesNewRoman", withString: "Helvetica")
            var newFontName: String?
            if lowerFont.contains("bold"){
                newFontName = "HelveticaNeue-Bold"
            }else if lowerFont.contains("italic"){
                newFontName = "HelveticaNeue-Italic"
            }else{
                newFontName = "HelveticaNeue"
            }
            
            //let newFontName = "Helvetica"
            //let newFontName = "HelveticaNeue-Light"
            //print(newFontName)
            let newFont = UIFont(name: newFontName!, size: fontSize)!
            
            //let newFont = UIFont.systemFontOfSize(fontSize)
            
            attributedString.removeAttribute(NSAttributedString.Key.font, range: range)
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.white , range: range)
            attributedString.addAttribute(NSAttributedString.Key.font, value: newFont, range: range)
        })
        attributedString.endEditing()
        
        return attributedString
    }
    
    static func toNSAttributedString(htmlString htmlEncodedString: String, fontSize: CGFloat) -> NSAttributedString?{
        let newString = htmlEncodedString.replacingOccurrences(of: "\n", with: "<br>")
        let encodedData = newString.data(using: String.Encoding.utf8)!
//        let attributedOptions : [String: AnyObject] = [
//            NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType as AnyObject,
//            NSCharacterEncodingDocumentAttribute: String.Encoding.utf8 as AnyObject,
//            
//        ]
        do{
//            var attributedString = try NSAttributedString(data: encodedData, options: attributedOptions, documentAttributes: nil)
//            var attributedString = NSAttributedString(string: htmlEncodedString, attributes: attributedOptions)
            var attributedString = try NSAttributedString(data: encodedData, options: [.documentType : NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
            attributedString = UIUtils.attributedStringWithFontSize(attributedString,fontSize: fontSize)
            return attributedString
        }catch{
            return nil
        }
        
    }
    
    public static func getContainerSizeFor(_ attrStr: NSAttributedString, labelSize viewSize: CGRect) -> CGRect{
        
        let width = viewSize.width
        var height = viewSize.height
        
        // whatever your desired width is
        var rect: CGRect = attrStr.boundingRect(with: CGSize(width: width, height: CGFloat(Float.greatestFiniteMagnitude) ), options: [.usesLineFragmentOrigin, .usesFontLeading], context: nil)
        
        
        if rect.height > height{
            height = rect.height//50
            rect = CGRect(x: rect.minX, y: rect.minY, width: rect.width, height: height*1.5)
            return rect
        }else{
            return viewSize
        }
    }
    
    
    public static func getLabelSizeFor(_ attrStr: String, labelSize viewSize: CGRect) -> CGRect{
        
        let width = viewSize.width
        var height = viewSize.height
        
        // whatever your desired width is
        var rect: CGRect = attrStr.boundingRect(with: CGSize(width: width, height: CGFloat(Float.greatestFiniteMagnitude) ), options: [.usesLineFragmentOrigin, .usesFontLeading], context: nil)
        
        
        if rect.height > height{
            height = rect.height//50
            rect = CGRect(x: rect.minX, y: rect.minY, width: rect.width, height: height*1.5)
            return rect
        }else{
            print(viewSize)
            return viewSize
        }
    }
    
    public static func loadCustomColorsAndImage(loadedFunction callback: @escaping () -> Void){
        
            ClientsAPI.getSettings { (bconfig: BConfig?, error: NSError?) in
                if let config = bconfig{
                    
                    if let imagePath = config.x3{
                        Settings.setColors(config)
                        Settings.Logo = imagePath
                        SwiftEventBus.post(Events.LOGO_CHANGED.rawValue)
                        BColor.reloadColors()
                        
                        callback()
                       
                    }else{
                        BColor.reloadColors()
                        callback()
                    }
                }else{
                    UIUtils.toast("No se pudieron cargar las preferencias")
                    BColor.reloadColors()
                    callback()
                }
            
            }
        
        
        
    }
    
    public static func clean(_ value: String?) -> String{
        if let val = value{
            return val
        }else{
            return ""
        }
    }
    
    public static func clean(_ value: Int?) -> Int{
        if let val = value{
            return val
        }else{
            return 0
        }
    }
    
 
    
    public static func convertToGrayScale(image: UIImage) -> UIImage{
        let monochromeFilter = CIFilter(name: "CIColorControls")!
        
        _ = UIColor(hexString: "#cccccc")!
        
       
        monochromeFilter.setValue(CIImage(image: image), forKey: kCIInputImageKey)
        
        monochromeFilter.setValue(0, forKey: "inputSaturation")
//        monochromeFilter.setValue(ciColor1, forKey: "inputColor1")
        
        
        let output = monochromeFilter.outputImage
        let context:CIContext = CIContext.init(options: nil)
        let cgimg = context.createCGImage(output!,from: output!.extent)
        
        let processedImage = UIImage(cgImage: cgimg!)
        return processedImage
    }
    
    public static func convertToGoldenScale(image: UIImage) -> UIImage{
        let monochromeFilter = CIFilter(name: "CIFalseColor")!
        let ciColor1 = CIColor(color: BColor.GoldPrimary)
        let ciColor0 = CIColor(color: BColor.GoldSecondary)
        
        monochromeFilter.setValue(CIImage(image: image), forKey: kCIInputImageKey)
        
        monochromeFilter.setValue(ciColor0, forKey: "inputColor0")
        monochromeFilter.setValue(ciColor1, forKey: "inputColor1")
        
        
        let output = monochromeFilter.outputImage
        let context:CIContext = CIContext.init(options: nil)
        let cgimg = context.createCGImage(output!,from: output!.extent)
        
        var processedImage = UIImage(cgImage: cgimg!)
        
        
        
        let secondFilter = CIFilter(name: "CIPhotoEffectChrome")!
       
        secondFilter.setValue(CIImage(image: processedImage), forKey: kCIInputImageKey)
        
        
        
        let secondOutput = secondFilter.outputImage
        let secondContext : CIContext = CIContext.init(options: nil)
        let secondCGImage = secondContext.createCGImage(secondOutput!,from: secondOutput!.extent)
        
        processedImage = UIImage(cgImage: secondCGImage!)
        
        
        
        
        return processedImage
    }
    
    
    public static func linearGradient(image: UIImage) -> CAGradientLayer{
        let myGradientLayer: CAGradientLayer = CAGradientLayer()
        myGradientLayer.startPoint = CGPoint(x: 0, y: 0)
        myGradientLayer.endPoint = CGPoint(x: 1, y: 1)
        let colors: [CGColor] = [
            UIColor.clear.cgColor,
            UIColor(red: 0, green: 0, blue: 0, alpha: 0.3).cgColor,
            UIColor(red: 1, green: 1, blue: 1, alpha: 0.5).cgColor,
            UIColor(red: 0, green: 0, blue: 0, alpha: 0.3).cgColor,
            UIColor.clear.cgColor ]
        myGradientLayer.colors = colors
        myGradientLayer.isOpaque = false
        myGradientLayer.locations = [0.0,  0.3, 0.5, 0.7, 1.0]
        
        
        return myGradientLayer
        
    }
    
    public static  func addSubView(_ subView:UIView, toView parentView:UIView) {
        parentView.addSubview(subView)
        
        var viewBindingsDict = [String: AnyObject]()
        viewBindingsDict["subView"] = subView
        parentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[subView]|",
            options: [], metrics: nil, views: viewBindingsDict))
        parentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[subView]|",
            options: [], metrics: nil, views: viewBindingsDict))
        
        subView.frame = parentView.frame
        subView.layoutIfNeeded()
        parentView.layoutIfNeeded()
        
    }
    
    public static func addShadowToCellView(_ item: UIView, opacity: CGFloat = 0.5){
        
        item.layer.masksToBounds = false
        item.layer.shadowColor = UIColor.black.cgColor
        item.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        
        item.layer.shadowOpacity = Float(opacity)
        
    }
    
    public static func toString(_ number: Int) -> String{
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.maximumFractionDigits = 0;
        let num = NSNumber.init(value: number)
        let s: String = formatter.string(from: num)!
        return s.replacingOccurrences(of: ",", with: ".")
    }
    
    public static func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    public static func resizeImage(_ image: UIImage) -> UIImage {
        
        var actualHeight: CGFloat = image.size.height
        var actualWidth: CGFloat = image.size.width
        let maxHeight: CGFloat = 300.0
        let maxWidth: CGFloat = 400.0
        var imgRatio: CGFloat = actualWidth / actualHeight
        let maxRatio: CGFloat = maxWidth / maxHeight
        let compressionQuality: CGFloat = 0.5
        //50 percent compression
        if actualHeight > maxHeight || actualWidth > maxWidth {
            if imgRatio < maxRatio {
                //adjust width according to maxHeight
                imgRatio = maxHeight / actualHeight
                actualWidth = imgRatio * actualWidth
                actualHeight = maxHeight
            }
            else if imgRatio > maxRatio {
                //adjust height according to maxWidth
                imgRatio = maxWidth / actualWidth
                actualHeight = imgRatio * actualHeight
                actualWidth = maxWidth
            }
            else {
                actualHeight = maxHeight
                actualWidth = maxWidth;
            }
        }
        
        let rect: CGRect = CGRect(x: 0.0, y: 0.0, width: actualWidth, height: actualHeight)
        
        UIGraphicsBeginImageContext(rect.size)
        image.draw(in: rect)
        let img: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        let imageData = img.jpegData(compressionQuality: compressionQuality)!
        UIGraphicsEndImageContext();
        
        return UIImage.init(data: imageData)!
        
    }
    
    public static func runOnUIThread(_ method: @escaping () -> Void){
        DispatchQueue.main.async(execute: {
            method()
        });
    }
    
    public static func showInDefaultModal(_ originController: UIViewController, controller: UIViewController, headerBarEnabled: Bool = true){
        let modalController = originController.storyboard?.instantiateViewController(withIdentifier: "DefaultModalViewController") as! DefaultModalViewController
        modalController.embeddedController = controller
        
        var controllerToPresent: UIViewController = controller
        if headerBarEnabled{
            let modalController = originController.storyboard?.instantiateViewController(withIdentifier: "DefaultModalViewController") as! DefaultModalViewController
            modalController.embeddedController = controller
            modalController.modalFinished = {
                originController.dismiss(animated: true, completion: nil)
            }
            
            if let mController = controller as? BModalViewController{
                if mController.modalFinished == nil{
                    mController.modalFinished = modalController.modalFinished
                }
            }
            controllerToPresent = modalController
        }
        
        originController.present(controllerToPresent, animated: true, completion: nil)
        
    }
    
    public static func getHeaderImageHeightForCurrentDevice() -> CGFloat {
        switch UIScreen.main.nativeBounds.height {
        case 2436 , 1792 , 2688: // iPhone X,Xr,Xs,Xs max
            return 120
        default: // Every other iPhone
            return 64
        }
    }
    
}
