//
//  MissionFinishViewController.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 6/1/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import UIKit
import Kingfisher
import AVFoundation

class MissionFinishViewController: BModalViewController {

    @IBOutlet weak var mainMessage: UILabel!
    @IBOutlet weak var complementMessage: UILabel!
    @IBOutlet weak var continueButton: BSecondaryButton!
    @IBOutlet weak var winImageView: AnimatedImageView!
    @IBOutlet weak var closeButton: UILabel!
    
    //CelebrationView
    @IBOutlet weak var mainCelebView: UIView!
    @IBOutlet weak var celebDarkView: UIImageView!
    @IBOutlet weak var imgCelebbg: UIImageView!
    @IBOutlet weak var iconCeleb: UIImageView!
    @IBOutlet weak var curveCelebView: UIView!
    @IBOutlet weak var lblCelebHEad: UILabel!
    @IBOutlet weak var lblCelebMsg: UILabel!
    @IBOutlet weak var innerCelebView1: UIView!
    @IBOutlet weak var innerCelebView2: UIView!
    @IBOutlet weak var btnListo: UIButton!
    @IBOutlet weak var lblCelebCreditMission: UILabel!
    @IBOutlet weak var lblCelebExpMission: UILabel!
    
    //FailedView
    @IBOutlet weak var curveFailView: UIView!
    @IBOutlet weak var lblFailbMsg: UILabel!
    @IBOutlet weak var iconFail: UIImageView!
    
    var passCredit : Int?
    var passExp : Int?
    
    var mission: Mission!
    var newTitles: [Title]?
    var newMedals: [Medal]?
    
    var failPages: [MissionPage]?
    var player = AVAudioPlayer()
    
    var shouldFinishMission: Bool = true
    var skipCheckAnswers = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationItem.setHidesBackButton(true, animated: false)
        self.closeButton.isHidden = true
        if let tbc = self.tabBarController as? TabBarController{
            tbc.enableBackButton(false, navController: nil)
        }
        self.view.backgroundColor = GlobalBgColor
            //UIColor(hexString: "#f7f7f7")
        
        self.winImageView.backgroundColor = GlobalBgColor
        //UIColor(hexString: "#f7f7f7")
        continueButton.layer.cornerRadius = 27.5
        
        continueButton.layer.masksToBounds = true
        
       // self.winImageView.tintColor = UIColor(hexString: "#f7f7f7")
        self.loadViewIfNeeded()
        self.view.layoutIfNeeded()
        
        //Celebview code
        mainCelebView.isHidden = true
        mainCelebView.backgroundColor = GlobalBgColor
        celebDarkView.image = celebDarkView.image?.withRenderingMode(.alwaysTemplate)
        celebDarkView.tintColor = GlobalBgColor?.darker(by: 13)
        
        imgCelebbg.image = imgCelebbg.image?.withRenderingMode(.alwaysTemplate)
        imgCelebbg.tintColor = GlobalBgColor?.lighter(by: 13)
        curveCelebView.layer.cornerRadius = 8
        curveCelebView.layer.masksToBounds = true
        
        curveFailView.layer.cornerRadius = 8
        curveFailView.layer.masksToBounds = true
        
        innerCelebView1.layer.cornerRadius = 17
        innerCelebView1.layer.masksToBounds = true
        
        innerCelebView2.layer.cornerRadius = 17
        innerCelebView2.layer.masksToBounds = true
        
        btnListo.layer.cornerRadius = 26
        btnListo.layer.masksToBounds = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let fp = failPages , fp.count > 0{
            self.toFailMissionMode()
        }else{
            self.toFinishMissionMode()
        }
    }
    
//    func createRewardModal(_ rewardName: String, imageUrl: String, credits: Int,passCredits: Int, passExp: Int) -> RewardPopupController{
//        let rewardController = storyboard!.instantiateViewController(withIdentifier: "RewardPopupController") as! RewardPopupController
//        rewardController.passCredit = passCredits
//        rewardController.passExp = passExp
//        rewardController.titleMessage = rewardName
//        rewardController.imageUrl = imageUrl
//        return rewardController
//    }
    
    func createRewardModal(_ rewardName: String, imageUrl: String, credits: Int) -> medalRewardViewController{
        let rewardController = storyboard!.instantiateViewController(withIdentifier: "medalRewardViewController") as! medalRewardViewController
        rewardController.credits = credits
        rewardController.titleMessage = rewardName
        rewardController.imageUrl = imageUrl
        return rewardController
    }
    
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationItem.setHidesBackButton(false, animated: false)
    }
    
    @IBAction func nextButtonPressed(_ sender: AnyObject) {
        
        
        if !self.shouldFinishMission{
            self.modalFinished?()
            self.continueButton.unlockButton()
            //self.dismiss(animated: true, completion: nil)   //dismiss with 2 views
            
        
        }else{
            //envio el form
            
            var modals: [medalRewardViewController] = [medalRewardViewController]()
            
             if let titles = self.newTitles , titles.count > 0{
                           for t in titles{
                               let controller = self.createRewardModal("¡Haz ganado el titulo \"\(t.name!)\"!", imageUrl: t.imageUrl!, credits: 0)
                               modals.append(controller)
                           }
             }
                       
             if let medals = self.newMedals , medals.count > 0{
                           for m in medals{
                               let controller = self.createRewardModal("¡Haz ganado la medalla \"\(m.name!)\"!", imageUrl: m.imageUrl!, credits: m.credits!)
                               modals.append(controller)
                           }
             }
            
            if modals.count > 0 {
                 let viewModalFinished = {
                    self.dismiss(animated: false, completion: {
                        if let nextModal = modals.popLast(){
                            nextModal.modalPresentationStyle = .overCurrentContext
                            self.present(nextModal, animated: false, completion: nil)
                        }else{
                            self.modalFinished?()
                            self.continueButton.unlockButton()
                        }
                    })
                }
                
                for m in modals {
                    m.modalFinished = viewModalFinished
                }
                
                let lastModal = modals.popLast()!
                lastModal.modalPresentationStyle = .overCurrentContext
                self.present(lastModal, animated: false, completion: {
                    
                })
            }else{
                self.skipCheckAnswers = true   //not to show answers
                //self.mission.assignAnswers()
                self.modalFinished?()
                self.continueButton.unlockButton()
                
            }
        }
    }
    
    fileprivate func toFailMissionMode(){
        isTapOnRow = ""
        isTapOnRowRadioBtn = ""
        self.shouldFinishMission = false
        self.winImageView.isHidden = true
        self.mainCelebView.isHidden = false
        self.curveFailView.isHidden = false
        self.curveCelebView.isHidden = true
        self.iconCeleb.isHidden = true
        self.iconFail.isHidden = false
        self.imgCelebbg.isHidden = true
        self.lblFailbMsg.text = "¡Casi has terminado! Has contestado de forma incorrecta algunas preguntas.¡Respóndelas correctamente para terminar la misión!"
        self.btnListo.setTitle("REVISAR PREGUNTAS INCORRECTAS", for: UIControl.State())
    }
    
    
    
    fileprivate func toFinishMissionMode(){
        
        // perform changes in condition to show mission finish design
        self.winImageView.isHidden = false
        self.iconFail.isHidden = true
        
        if self.mission.Target == .quiz {
            self.shouldFinishMission = true
            
            mainCelebView.isHidden = true
            self.continueButton.setTitle("LISTO", for: UIControl.State())
            self.continueButton.backgroundColor = UIColor.init(red: 71/255, green: 210/255, blue: 160/255, alpha: 1)
            let msg = "Test finalizado\n Lograste un \(self.mission.Grade)%"
            self.mainMessage.text = msg.uppercased()
            let grade = self.mission.Grade
            if grade == 100 {
                self.complementMessage.text = "¡Felicitaciones! Tienes un puntaje perfecto"
                self.winImageView.image = UIImage(imageLiteralResourceName: "Happy_face")
            }else if grade > 75 {
                self.complementMessage.text = "¡Muy bien! sigue así"
                self.winImageView.image = UIImage(imageLiteralResourceName: "Happy_face")
            }else if grade > 50 {
                self.complementMessage.text = "¡Bien! y esperamos que la próxima vez sea mejor"
                self.winImageView.image = UIImage(imageLiteralResourceName: "Sad_face")
            }else if grade > 25{
                self.complementMessage.text = "Completaste el test, pero ¡puedes hacerlo mejor!"
                self.winImageView.image = UIImage(imageLiteralResourceName: "Sad_face")
            }else{
                self.complementMessage.text = "¡Puedes hacerlo mejor!"
                self.winImageView.image = UIImage(imageLiteralResourceName: "Sad_face")
            }
        }
        else{
            self.continueButton.setTitle("LISTO", for: UIControl.State())
            self.mainCelebView.isHidden = false
            self.curveFailView.isHidden = true
            self.curveCelebView.isHidden = false
            
            self.lblCelebExpMission.text = "\(UIUtils.clean(passExp))"
            self.lblCelebCreditMission.text = "\(UIUtils.clean(passCredit))"
            
            UserDefaults.standard.set("\(UIUtils.clean(passExp))", forKey: "passExp")
            UserDefaults.standard.set("\(UIUtils.clean(passCredit))", forKey: "passCredit")
            
            
            //celebration new view
            if let nr = self.mission.needsRevision, nr == true {   //target elearning
                 self.lblCelebHEad.text = "¡YA CASI HAS TERMINADO"
                 self.lblCelebMsg.text = "Ahora solo debes esperar a que la misión sea revisada"
                
            }
            else{
                self.shouldFinishMission = true
                self.lblCelebHEad.text = "¡FELICITACIONES! MISION COMPLETADA"
                self.lblCelebMsg.text = "Acabas de acumular"
                
                //bonus if else
            }
        }
    
//        if(self.mission.Target == .elearning){
//
//        }else{
//            self.continueButton.setTitle("VER RESPUESTAS", for: UIControl.State())
//            self.closeButton.isHidden = false
//        }
    }
    
    @IBAction func closeButtonClicked(_ sender: Any) {
        //print("Click !!!!")
        self.skipCheckAnswers = true
        self.continueButton.lockButton()
        self.modalFinished?()
    }
    
    func music()
    {
        if let url = Bundle.main.url(forResource: "win_music", withExtension: "wav"){
            do
            {
                player = try AVAudioPlayer(contentsOf: url, fileTypeHint: nil)
            }
            catch let error as NSError { print(error.description) }
            //player.play()
        }
    }
}
