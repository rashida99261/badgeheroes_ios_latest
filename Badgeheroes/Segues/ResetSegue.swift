//
//  ResetSegue.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 6/3/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import UIKit

class ResetSegue: UIStoryboardSegue {

    override func perform() {
        let sourceViewController: UIViewController = (self.sourceViewController )
        let destinationController: UIViewController = (self.destinationViewController )
        
        // Pop to root view controller (not animated) before pushing
        sourceViewController.navigationController?.popToRootViewControllerAnimated(false)
        sourceViewController.navigationController?.pushViewController(destinationController, animated: true)
        

    }
}
