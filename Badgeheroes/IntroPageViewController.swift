//
//  IntroPageViewController.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 6/16/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import UIKit

class IntroPageViewController: UIViewController {
    
    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    
    override var shouldAutorotate : Bool {
        return false
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.portrait
    }
    
    @IBAction func onEnter(_ sender : Any)
    {
        print("on enter")
    }
}
