//
//  BModalViewController.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 6/5/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import UIKit

class BModalViewController: BaseUIViewController {
    var modalFinished: (() -> Void)?
    var enableNavbar: ((Bool) -> Void)?
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let anotherModalController = segue.destination as? BModalViewController{
            anotherModalController.modalFinished = self.modalFinished
        }
    }
}
