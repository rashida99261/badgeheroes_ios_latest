//
//  BPageViewController.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 6/1/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import UIKit



class BPagesViewController: UIViewController {

    var creditPass : Int?
    var expPass : Int?
    
    var mission: Mission?
    var userAnswers: [UserAnswer]?
    var missionPages = [MissionPage]()
    var originalMissionPages = [MissionPage]()
    var failMissionPages = [MissionPage]()
    
    var NmissionPage: MissionPage!
    
    var currentPageContainerController: BPageViewContainerController!
    var index = 0
    
    
    var pageControlX: CGFloat!
    var pageControlY: CGFloat!
    var pageControlWidth: CGFloat!
    var pageControlHeight: CGFloat!
    var watchRightAnswersMode = false
    var rightAnswersWatched = false
    
    var watchUserAnswersMode = false
    var userAnswersWatched = false
    
    var isFail = ""
    @IBOutlet weak var nextButton: UIButton!
    
    @IBOutlet weak var backButton: UIButton!
    
    @IBOutlet weak var pageControl: UIPageControl!

    @IBOutlet weak var containerView: UIView!
    
    var newMedals: [Medal]?
    var newTitles: [Title]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = GlobalBgColor
        
        self.nextButton.setTitleColor(UIColor.white, for: UIControl.State.normal)
        self.nextButton.backgroundColor = GlobalBgColor?.darker(by: 13)
        self.nextButton.layer.cornerRadius = 20
        self.nextButton.clipsToBounds = true
        
        if let m = mission {
            
            
            if self.missionPages.count == 0
            {
                missionPages = m.missionPages!
                originalMissionPages = m.missionPages!
            }
        }
        
        self.pageControl.isHidden = true
     //   self.pageControl.layoutMarginsDidChange()
      //  self.pageControl.superview?.layoutIfNeeded()
      //  self.pageControl.superview?.layoutMarginsDidChange()
        self.view.layoutIfNeeded()
        self.reloadPages()
//
        self.pageControlX = self.pageControl.frame.minX
        self.pageControlY = self.pageControl.frame.minY
        self.pageControlWidth = self.pageControl.frame.width
        self.pageControlHeight = self.pageControl.frame.height
        
       // self.backButton.isHidden = true
        //self.pageControl.backgroundColor = UIColor.clear
        
       // self.pageControl.pageIndicatorTintColor =  UIColor.gray
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //Si no se hace esto, el pageControl salta a punta del cerro
        let newPageControlX = self.view.frame.width/2 - pageControlWidth/2
        self.pageControl.frame = CGRect(x: newPageControlX , y: pageControlY, width: pageControlWidth, height: pageControlHeight)
    }
    
    @IBAction func pageControlChanged(_ sender: AnyObject) {
    }
    
    @IBAction func nextButtonPressed(_ sender: AnyObject) {
        showNextPage()
    }
    
    @IBAction func backButtonPressed(_ sender: AnyObject) {
//        showPrevPage()
    }
    
//    @IBAction func swipeRight(_ sender: AnyObject) {
////        showPrevPage()
//    }
//
//    @IBAction func swipeLeft(_ sender: AnyObject) {
//        showNextPage()
//    }
    
    fileprivate func reloadPages(){
        self.failMissionPages = [MissionPage]()
        self.index = 0
        self.pageControl.numberOfPages = self.missionPages.count
        if self.index == self.missionPages.count - 1{
            self.nextButton.setTitle("Finalizar", for: UIControl.State())
        }else{
            
            self.nextButton.setTitle("Siguiente", for: UIControl.State())
        }
        setSubController(self.index)
    }
    
    fileprivate func sendAnswers(){
        //envio el form
        UIUtils.toast("Enviando respuestas...")
        self.nextButton.isHidden = true
        MissionsAPI.finishMission(self.mission!, callback: { (response: MissionFinishedResponse?, error: NSError?) in
            
            
            if error != nil{
                self.nextButton.isHidden = false
                UIUtils.toast("Hubo un problema de conexion")
            }else{
                if let newMedals = response?.newMedals{
                    self.newMedals = newMedals
                }
                
                if let newTitles = response?.newTitles{
                    self.newTitles = newTitles
                }
                UIUtils.toast("Respuestas enviadas")
            }
            self.toMissionFinishView()
        })
    }
    
    fileprivate func showNextPage(){
        
        let currentPageController = self.currentPageContainerController
      //  _ = currentPageController?.completeMissionPage()
        
        print(currentPageController?.isPageCorrect() ?? false)
        
        //swati
               _ = currentPageController?.completeMissionPage()
                    
                    if !(currentPageController?.isPageCompleted())!{
                        currentPageController?.showErrorMessage()
                        return
                    }
        //swati end
               
               
        
        
        if self.mission?.Target != .quiz && !(currentPageController?.isPageCorrect())!{
            UIUtils.toast("¡Te has equivocado en una de las preguntas!")
            if(isTapOnRow == "" && isTapOnRowRadioBtn == ""){
                return
            }
            else{
                 self.failMissionPages.append((currentPageController?.missionPage)!)
                 isTapOnRow = ""
                 isTapOnRowRadioBtn = ""
            }
        }
        
        
        //
        let index = self.index + 1
        if(index < self.missionPages.count){
            //self.backButton.hidden = false
            
            cycleFromViewController(index)
            self.index = index
            self.pageControl.currentPage = index
            
            if self.index == self.missionPages.count - 1{
                self.nextButton.setTitle("Finalizar", for: UIControl.State())
            }else{
                self.nextButton.setTitle("Siguiente", for: UIControl.State())
            }
        }else{
            
           // self.backButton.isHidden = true
            
            //e-learning with failMissionPages
            //e-learning with failMissionPages = 0
            //quiz answered
            //quiz - answers watched
            //quiz require watchAnswers=true for send answers
            
           
            
            
            if(self.watchUserAnswersMode){
                self.dismissMission()
                return
            }
            
            if (self.mission?.Target == .elearning && self.failMissionPages.count == 0 ){
                sendAnswers()
            }else if (self.mission?.Target == .elearning && self.failMissionPages.count > 0){
                self.toMissionFinishView()
            }else if (self.mission?.Target == .quiz && !self.watchRightAnswersMode){
                self.watchRightAnswersMode = true
                sendAnswers()
            }else if (self.mission?.Target == .quiz && self.watchRightAnswersMode){
                self.rightAnswersWatched = true
                self.dismissMission()
            }
        }
    }
    
    fileprivate func dismissMission(){
        let controllers: [AnyObject] = self.navigationController!.viewControllers
        for controller in controllers{
            
            if (watchUserAnswersMode){
                if(controller is AnswersViewController){
                    self.dismiss(animated: true, completion: nil)
                    self.navigationController!.popToViewController(controller as! UIViewController, animated: true)
                    return
                }
            }else{
                if (controller is MissionsViewController || controller is HeroViewController ) {
                    self.dismiss(animated: true, completion: nil)
                    self.navigationController!.popToViewController(controller as! UIViewController, animated: true)
                    return
                }
            }
        }
    }
    
    fileprivate func toMissionFinishView(){
        let finishController = storyboard!.instantiateViewController(withIdentifier: "MissionFinishViewController") as! MissionFinishViewController
        finishController.modalPresentationStyle = .fullScreen
        finishController.passCredit = self.creditPass
        finishController.passExp = self.expPass
        finishController.mission = self.mission
        finishController.failPages = self.failMissionPages
        finishController.newTitles = self.newTitles
        finishController.newMedals = self.newMedals
        
        finishController.modalFinished = {
            if self.failMissionPages.count > 0{
                
                self.isFail = "fail"
                self.currentPageContainerController.willMove(toParent: nil)
                self.currentPageContainerController.view.removeFromSuperview()
                self.currentPageContainerController.removeFromParent()
                self.currentPageContainerController.informationLabel.text = ""
                self.missionPages = self.failMissionPages
                self.reloadPages()
                self.dismiss(animated: true, completion: nil)
                
            }else{
                if(self.mission?.Target == .quiz && finishController.skipCheckAnswers){
                    self.rightAnswersWatched = true
                    self.watchRightAnswersMode = true
                }
                
                if(self.mission?.Target == .quiz && self.watchRightAnswersMode && !self.rightAnswersWatched){
                    //self.missionPages = self.originalMissionPages
                    //self.nextButton.isHidden = false
                    //self.reloadPages()
                   // self.dismiss(animated: true, completion: nil)
                    
                    self.dismissMission()
                    
                }else{
                    self.dismissMission()
                }
            }
        }
        self.present(finishController, animated: true, completion: nil)
    }
    
    fileprivate func showPrevPage(){
        
        self.nextButton.setTitle("Siguiente", for: UIControl.State())
        let index = self.index - 1
        if(index >= 0) {
            
            cycleFromViewController(index)
            self.index = index
            self.pageControl.currentPage = index
            
            if self.index == 0{
               // self.backButton.isHidden = true
            }
        }
    }

    fileprivate func setSubController(_ index: Int){
        
        if(index >= 0 && index < self.missionPages.count){
            
            let newController = createPageViewController(index)
            self.currentPageContainerController.removeFromParent()
            self.pageControl.currentPage = index
            newController.loadViewIfNeeded()
            _ = newController.view.translatesAutoresizingMaskIntoConstraints
            newController.view.translatesAutoresizingMaskIntoConstraints = false
            self.addChild(newController)
            UIUtils.addSubView(newController.view, toView: self.containerView)
            newController.loadView()
            newController.didMove(toParent: self)
            newController.viewDidLoad()
        }        
    }
    
    func cycleFromViewController(_ newIndex: Int) {
        let prevIndex = self.index
        
        let oldVC =  currentPageContainerController
        let newVC = createPageViewController(newIndex)
        
        // Prepare the two view controllers for the change.
        oldVC?.willMove(toParent: nil)
        self.addChild(newVC)
        
        let newMinX = self.containerView.frame.minX
        let newMinY = self.containerView.frame.minY
        let newMaxX = self.containerView.frame.maxX
        //let newMaxY = self.containerView.frame.maxY
        let newWidth = self.containerView.frame.width
        let newHeight = self.containerView.frame.height
        
        let oldEndFrame: CGRect
        
        let newEndFrame: CGRect = CGRect(x: newMinX, y: newMinY, width: newWidth , height: newHeight)
        
        if prevIndex < newIndex{
            //Estamos avanzando
            
            //newViewStartFrame
            newVC.view.frame = CGRect(x: newMaxX + 10, y: newMinY, width: newWidth, height: newHeight)
            //oldViewEndFrame
            oldEndFrame = CGRect(x: newMinX - newWidth - 10, y: newMinY, width: newWidth, height: newHeight)
        }else{
            //Estamos retrocediendo
            
            //newViewStartFrame
            newVC.view.frame = CGRect(x: newMinX - newWidth - 10, y: newMinY, width: newWidth, height: newHeight)
            //oldViewEndFrame
            oldEndFrame = CGRect(x: newMaxX + 10, y: newMinY, width: newWidth, height: newHeight)
        }
        
        
        self.transition(from: oldVC!, to: newVC, duration: 0.1, options: [], animations: {() -> Void in
            // Animate the views to their final positions.
            newVC.view.frame = newEndFrame
            oldVC?.view.frame = oldEndFrame
            }, completion: {(finished: Bool) -> Void in
                // Remove the old view controller and send the final
                // notification to the new view controller.
                oldVC?.removeFromParent()
                newVC.didMove(toParent: self)
        })
    }
    
    fileprivate func createPageViewController(_ index: Int) -> UIViewController{
        
//        if(isFail == "fail"){
//            currentPageContainerController.removeFromParent()
//        }
        
            let missionPage = self.missionPages[index]
            currentPageContainerController = self.storyboard?.instantiateViewController(withIdentifier: "BPageViewContainerController") as? BPageViewContainerController
            currentPageContainerController.totalPage = self.missionPages.count
            currentPageContainerController.pagePosition = index
            currentPageContainerController.missionPage = missionPage
            currentPageContainerController.missionId = self.mission?.id
            currentPageContainerController.watchRightAnswers = self.watchRightAnswersMode
            currentPageContainerController.watchUserAnswers = self.watchUserAnswersMode
            return currentPageContainerController
    }
}
