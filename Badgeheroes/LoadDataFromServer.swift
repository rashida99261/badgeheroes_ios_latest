//
//  LoadDataFromInternet.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 5/27/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import Foundation

protocol LoadDataFromServer{
    
    func loadData()
}