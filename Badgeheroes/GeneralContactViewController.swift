//
//  GeneralContactViewController.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 6/13/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import UIKit
//var isOpenFrstTym = ""

class GeneralContactViewController: BModalViewController, UITextViewDelegate  {

    @IBOutlet weak var textFieldButton: UITextView!
    @IBOutlet weak var navBarView: UIView!
   // @IBOutlet weak var userNameLabel: UILabel!
   // @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var commentsTextField: UITextView!
    @IBOutlet weak var btnEnviar: UIButton!
    
    
    
    let placeholder = "Escribe tus comentarios aquí"
    override func viewDidLoad() {
        super.viewDidLoad()
       
     //   self.userNameLabel.text = Settings.FullName
     //   userImage.kf.setImage(with: URL(string: Settings.ImageUrl)!)
        //UIUtils.toCircleImage(userImage)
       // self.view.backgroundColor = BColor.SecondaryBar
        
        commentsTextField.delegate = self
        commentsTextField.text = placeholder
        commentsTextField.textColor = UIColor.lightGray
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navBarView.backgroundColor = GlobalBgColor?.darker(by: 13)
        self.btnEnviar.backgroundColor = GlobalBgColor?.darker(by: 13)

        commentsTextField.layer.borderColor = UIColor.black.cgColor;
        commentsTextField.layer.borderWidth = 2.0;
        //commentsTextField.layer.cornerRadius = 5.0;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func acceptButtonPressed(_ sender: AnyObject) {
        
        let wordsaar = self.commentsTextField.text.split(separator: " ")
        if (wordsaar.count >= 5)
        {
            UsersAPI.contact(self.commentsTextField.text, commentType: UsersAPI.CommentType.problem) { (response, error) in
                //print("ressss = \(response)-------\(error)")
                UIUtils.toast("Sus comentarios han sido enviados")
                if let callback = self.modalFinished {
                    callback()
                }
            }
        }
        else {
            UIUtils.toast("El Contenido del mensaje es muy corto")
        }
    }

    @IBAction func backButtonPressed(_ sender: AnyObject) {
        if let callback = self.modalFinished{
            callback()
        }
    }
    
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if (textView.text! == placeholder) {
            textView.text = ""
            textView.textColor = UIColor.black
            //optional
        }
        textView.becomeFirstResponder()
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if (textView.text! == "") {
            textView.text = placeholder
            textView.textColor = UIColor.lightGray
            //optional
        }
        textView.resignFirstResponder()
    }
    
    @IBAction func clickOnBackBtn(_ sender : UIButton){
        self.dismiss(animated: true, completion: nil)
      //  self.navigationController?.popViewController(animated: true)
    }
}
