//
//  MyPrizesViewController.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 7/20/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//


import UIKit
import SwiftEventBus

class MyPrizesViewController: BaseUIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var navBarView: UIView!
    @IBOutlet weak var creditsLabel: UILabel!
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var navBarViewHeightConstraint : NSLayoutConstraint!
    
    var userId: Int?
    var prizes: [Prize] = [Prize]()
    var lastPrizeId = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.allowsSelection = false
        self.reloadLogo()
        
        // Do any additional setup after loading the view.
        
        SwiftEventBus.onMainThread(self, name: Events.LOGO_CHANGED.rawValue) { result in
            self.reloadLogo()
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if let id = self.userId{
            UsersAPI.getUser(userId: id, { (serverUser: User?, error:  NSError?) in
                if let su = serverUser{
                    self.loadUser(su)
                }
            })
        }else{
            UsersAPI.getProfile { (serverUser: User?, error: NSError?) in
                if let su = serverUser{
                    self.loadUser(su)
                }
            }
        }
        
        PrizesAPI.myPrizes { (prizes, error) in
            print("przzzzzzzz = \(prizes)")
            
            if let ps = prizes{
                self.prizes = ps
                self.tableView.reloadData()
            }
        }
        
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navBarView.backgroundColor = BColor.PrimaryBar
        GlobalBgColor = BColor.PrimaryBar
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return prizes.count
    }

    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let item = tableView.dequeueReusableCell(withIdentifier: "BPrizeRowCell", for: indexPath) as! BPrizeRowCell
        
        let index: Int = (indexPath as NSIndexPath).row
        
        if (index % 2 == 0)
        {
            item.viewBg.backgroundColor = UIColorFromHex(rgbValue: 0xEEEEEE)
            item.viewVredit.backgroundColor = UIColor.white
        }
        else {
            item.viewBg.backgroundColor = UIColor.white
            item.viewVredit.backgroundColor = UIColorFromHex(rgbValue: 0xEEEEEE)
        }
        
        let prize = prizes[index]
        
        item.prizeName.text = prize.name
        if let url = prize.imageUrl{
            
            item.prizeImage.kf.setImage(with: URL(string: url)!)
        }
        
        switch prize.State {
        case .waiting:
            item.toWaiting()
        case .accepted:
            item.toAccepted()
        case .rejected:
            item.toRejected()
        default:
            print("State not found")
        }
        
        item.prizeCredits.text = "\(UIUtils.clean(prize.credits))"
        
        return item
    }
    
   
    
    
    func loadUser(_ user: User){
        self.creditsLabel.text = "\(UIUtils.clean(user.credits))"
    }
    
    func reloadLogo(){
        UIUtils.loadLogo(logoImageView)
    }
    
    @IBAction func clickONBack(_ sender : UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
}
