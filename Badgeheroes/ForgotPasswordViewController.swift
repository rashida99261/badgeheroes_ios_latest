//
//  ForgotPasswordViewController.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 5/25/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: BModalViewController {

    
    @IBOutlet weak var emailLabel: UILabel!
   // @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var acceptButton: BPrimaryButton!
    
    var email = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // UIUtils.toCircleImage(userImage)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
       // userImage.kf.setImage(with: URL(string: Settings.ImageUrl)!)
        emailLabel.text = email
    }
    
    @IBAction func acceptButtonPressed(_ sender: AnyObject) {
        let email = emailLabel.text!
        
        UsersAPI.recoverPassword(email: email) { (apiResponse: ApiResponse?, error: NSError?) in
            if let response = apiResponse{
                if let status = response.status{
                    if status == "OK"{
                        UIUtils.toast("Se ha enviado un correo a tu email")
                        let _ = self.navigationController?.popViewController(animated: true)
                        return
                    }
                }
            }
            self.acceptButton.unlockButton()
            UIUtils.toast("No se pudo conectar con el servidor")
        }
    }
    
    @IBAction func clickOnBackBtn(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
}
