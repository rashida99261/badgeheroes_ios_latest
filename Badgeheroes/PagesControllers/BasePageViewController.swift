//
//  BasePageViewController.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 6/1/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import UIKit

class BasePageViewController: UIViewController, MissionPageController {
    

    
    var missionPage: MissionPage!
    var pagePosition: Int!
    var pageView: UIView!
    var missionId: Int!
    var watchRightAnswers: Bool! = false
    var watchUserAnswers: Bool! = false
    
    override func viewDidAppear(_ animated: Bool) {
        self.view.layoutIfNeeded()
    }
    
    func completeMissionPage() -> MissionPage {
        return missionPage
    }
    
    func isPageCompleted() -> Bool {
        return true
    }
    
    func isPageCorrect() -> Bool{
        return true
    }
    
    func saveCurrentWrongAnswers() -> Bool{
        return false
    }
    
    func loadDefault() {
        
    }
    
    func showErrorMessage(){
        UIUtils.toast("Hay campos requeridos no completados en esta página")
    }
}
