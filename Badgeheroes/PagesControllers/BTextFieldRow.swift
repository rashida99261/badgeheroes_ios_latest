//
//  BTextFieldRow.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 6/24/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import UIKit

class BTextFieldRow: BBaseRow{
    
    
    @IBOutlet weak var textField: UITextField!
    
    
    override func completePage() {
        if self.textField.text != ""{
            self.pageElement.answerText = self.textField.text
            self.pageElement.isAnswered = true
        }
    }
    
    override func loadDefault(){
        if let ans = pageElement.answerText{
            self.textField.text = ans
        }
    }
    
}