//
//  CheckinViewController.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 5/26/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class CheckinViewController: BasePageViewController, MKMapViewDelegate, CLLocationManagerDelegate {
    @IBOutlet weak var mapView: MKMapView!
    
    var lat: Double?
    var long: Double?
    
    var locationManager: CLLocationManager?
    var annotation = MKPointAnnotation()
    
    @IBOutlet weak var checkinButton: BSecondaryButton!
    
    override func viewDidLoad() {
        
    }
    

    override func viewWillAppear(_ animated: Bool) {
        
        startLocationManager()
        if(watchUserAnswers){
            self.checkinButton.lockButton()
        }
    }
    
    
    func startLocationManager(){
       // showSettings()
       
        if let lm = locationManager{
            lm.stopUpdatingLocation()
        }
        locationManager = CLLocationManager()
        
//swati
//        if (CLLocationManager.locationServicesEnabled())
//        {
//            switch CLLocationManager.authorizationStatus() {
//            case .notDetermined, .restricted, .denied:
//                showSettings()
//                self.checkinButton.unlockButton()
//                return
//
//            case .authorizedAlways, .authorizedWhenInUse:
//                locationManager?.delegate = self
//                locationManager?.desiredAccuracy = kCLLocationAccuracyBest
//                locationManager?.requestWhenInUseAuthorization()
//                locationManager?.startUpdatingLocation()
//
//            }
//        }swati
        
        
           if (CLLocationManager.locationServicesEnabled())
                {
       
                    self.mapView.addAnnotation(annotation)
                               locationManager?.delegate = self
                               locationManager?.desiredAccuracy = kCLLocationAccuracyBest
                               locationManager?.requestWhenInUseAuthorization()
                               locationManager?.startUpdatingLocation()
                            //swati
                }
            
    }
    
    func showSettings(){
        let alertController = UIAlertController (title: "BadgeHeroes", message: "Para completar esta misión se necesitan permisos de geolocalización. Solo se utilizarán para completar las misiónes de localización.", preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: "Ir a configuración", style: .default) { (_) -> Void in
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.openURL(settingsUrl)
                
            }
        }
        alertController.addAction(settingsAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func checkInButtonPressed(_ sender: AnyObject) {
        
        
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                showSettings()
                self.checkinButton.unlockButton()
                return
                
            case .authorizedAlways, .authorizedWhenInUse:
                if let lat = self.lat, let long = self.long{
                    let center = CLLocationCoordinate2D(latitude: lat, longitude: long)
                    let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
                    self.mapView.setRegion(region, animated: true)
                    annotation.coordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
                    UIUtils.toast("Haz hecho check-in correctamente")
                    return
                    
                }
            }
        }else{
            UIUtils.toast("No se pudo detectar tu ubicación. Verifica que tengas prendido tu GPS")
            self.checkinButton.unlockButton()
        }
        
      
    }
    override func loadDefault() {
        if let lat = self.missionPage.lat, let long = self.missionPage.lon{
            let center = CLLocationCoordinate2D(latitude: lat, longitude: long)
            
            let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
            
            self.mapView.setRegion(region, animated: true)
            
            annotation.coordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
        }
        
    }
    
    override func completeMissionPage() -> MissionPage {
        if let lat = self.lat, let long = self.long{
            self.missionPage.lat = lat
            self.missionPage.lon = long
        }
        
        return missionPage
    }
    
    override func isPageCompleted() -> Bool {
        if self.watchUserAnswers{
            return true
        }
        
        if let _ = self.lat, let _ = self.long{
            return true
        }
        return false
    }
    
   
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        
       // let location = locations.last!
        
        
        
      //  self.mapView.addAnnotation(annotation)
        
        
       // self.mapView.removeAnnotation(newPin)
        
        let location = locations.last! as CLLocation
        
        self.lat = location.coordinate.latitude
        self.long = location.coordinate.longitude
        let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        //set region on the map
        self.mapView.setRegion(region, animated: true)
        annotation.coordinate = location.coordinate
        self.mapView.addAnnotation(annotation)
    }
}
