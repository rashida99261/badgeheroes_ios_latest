//
//  MessageViewController.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 5/26/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import UIKit

class MessageViewController:BasePageViewController {
    
    @IBOutlet weak var messageLabel: PaddingLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
       // let strInformation = String((missionPage.information?.filter { !"\n".contains($0) })!)
        
       // print(strInformation)
        self.messageLabel.text = "\(missionPage.information!)"
        
        
//        var labelHeight: CGFloat?
//        var newSize: CGRect?
//        if let t = text{
//            let text = UIUtils.toNSAttributedString(htmlString: t, fontSize: 17)
//            self.messageLabel.attributedText = text
//            newSize = UIUtils.getContainerSizeFor(text!, labelSize: self.messageLabel.frame)
//            labelHeight = newSize!.height
//        }
//
//        if labelHeight == nil{
//            labelHeight = self.messageLabel.frame.height
//        }
//        self.messageLabel.sizeToFit()
//        self.messageLabel.frame = CGRect(x: 0, y: 0, width: cf.width, height: labelHeight!)
        
    }
    
       
}


@IBDesignable class PaddingLabel: UILabel {

    @IBInspectable var topInset: CGFloat = 0
    @IBInspectable var bottomInset: CGFloat = 0
    @IBInspectable var leftInset: CGFloat = 0
    @IBInspectable var rightInset: CGFloat = 0

    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
        super.drawText(in: rect.inset(by: insets))
    }

    override var intrinsicContentSize: CGSize {
        let size = super.intrinsicContentSize
        return CGSize(width: size.width + leftInset + rightInset,
                      height: size.height + topInset + bottomInset)
    }
}
