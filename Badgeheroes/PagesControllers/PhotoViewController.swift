//
//  PhotoViewController.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 5/26/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import UIKit
import ALCameraViewController

class PhotoViewController:BasePageViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    var imageUrl: String?
    var img: UIImage?
    
  //  @IBOutlet weak var progressBar: UIProgressView!

    //@IBOutlet weak var takePictureButton: BSecondaryButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
       // progressBar.setProgress(0, animated: false)
       // progressBar.isHidden = true
        self.imageUrl = self.missionPage.imageUrl
        if let i = img{
            self.img = i
        }
        if(watchUserAnswers){
          //  self.takePictureButton.lockButton()
        }
    }
    
    override func loadDefault() {
        if let url = self.missionPage.fileUrl{
            self.imageView.contentMode = .scaleAspectFit
            self.imageView.kf.setImage(with: URL(string: url))
        }
        
    }
    
    
    
    @IBAction func takePicturePressed(_ sender: AnyObject) {
        let cameraViewController = CameraViewController() { (image: UIImage?,asset: AnyObject?) in
            if let img = image{
                let compressImg = UIUtils.resizeImage(img)
                self.img = compressImg
                let fileName = "mission_user_states/\(self.missionId!)/mission_pages/\(self.missionPage.id!)/\(Int(NSDate().timeIntervalSince1970)).jpeg"
                let data = compressImg.jpegData(compressionQuality: 0.5)
                self.imageView.contentMode = .scaleAspectFit
                self.imageView.image = compressImg
               // self.progressBar.isHidden = false
                AmazonAPI.uploadFile(fileName, data: data!, progressCallback: self.uploadProgressCallback , finishCallback: self.uploadFinishedCallback)
                
                self.dismiss(animated: false, completion: nil)
            }else{
                self.dismiss(animated: false, completion: nil)
            }
           // self.takePictureButton.unlockButton()
        }
        
        present(cameraViewController, animated: false){
        }
    }
    
    override func completeMissionPage() -> MissionPage {
        self.missionPage.imageUrl = self.imageUrl
        return self.missionPage
    }
    
    override func isPageCompleted() -> Bool {
        if self.watchUserAnswers{
            return true
        }
        
        if let iUrl = self.missionPage.imageUrl{
            if iUrl != ""{
                return true
            }
        }
        
        return false
    }
    
    override func showErrorMessage(){
        UIUtils.toast("La imagen aún no se ha subido")
    }
    
    func uploadProgressCallback(_ bytesSent: Int64, totalBytesSent: Int64, totalBytesExpectedToSend: Int64)
    {
        DispatchQueue.main.async {
          //  let num = Double(totalBytesSent)
            //let den = Double(totalBytesExpectedToSend)
           //let progress = Float(num/den)
//            if self.progressBar.progress < progress{
//                self.progressBar.setProgress(progress, animated: true)
//            }
            
        }
        
    }
    
    func uploadFinishedCallback(_ error: NSError?, exception: String?, url: URL?){
        if let sUrl = url{
            self.imageUrl = sUrl.absoluteString
            //self.imageView.kf_setImageWithURL((sUrl), optionsInfo: [.ForceRefresh])
            
            OperationQueue.main.addOperation {
                if let i = self.img{
                    self.imageView.image = i
                }
                //            DispatchQueue.main.sync(execute: {
                //                self.progressBar.setProgress(0, animated: false)
                //                self.progressBar.isHidden = true
                //            })
                UIUtils.toast("Se ha actualizado la imagen correctamente")
            }
        }else{
            UIUtils.toast("Hubo un error al subir la imagen")
        }
    }
}
