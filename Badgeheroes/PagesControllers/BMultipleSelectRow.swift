//
//  BMultipleSelectRow.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 6/24/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import UIKit

class BMultipleSelectRow: BBaseRow, UITableViewDelegate, UITableViewDataSource{
    
    @IBOutlet weak var tblView: newTableView!
    @IBOutlet weak var hgtTblView: NSLayoutConstraint!
    
    
    fileprivate var size: CGSize?
     var totalHgt: CGFloat = 0.0
    
    var hideKeyboard: (() -> Void)?
    
    var watchRightAnswers: Bool!
    var watchUserAnswers: Bool!
    
    var optionRows: [BPageElementOptionRow] = [BPageElementOptionRow]()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style , reuseIdentifier: reuseIdentifier)
       // setUpTable()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        //setUpTable()
      
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        tblView.delegate = self
        tblView.dataSource = self

        // Initialization code
       // setUpTable()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        if let count = pageElement?.pageElementOptions?.count{
            return count
        }
        return 0
    }
    
    
    
   
    
    
//    override func updateViewConstraints() {
//       // tableHeightConstraint.constant = tableView.contentSize.height

//
//        super.updateViewConstraints()
//    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let row = self.optionRows[indexPath.row]
        //        let newSize = UIUtils.getLabelSizeFor(row.blabel.text!, labelSize: row.blabel.frame)
//        let hg = newSize.height + 16
//        totalHgt = totalHgt + hg
//        print("total--\(totalHgt)")
//        if(indexPath.row == Int(self.optionRows.count) - 1){
//            self.tableViewHght.constant = totalHgt //self.tableView.contentSize.height
//            self.mainViewHght.constant = totalHgt + 20
//           // mainHghtCons = totalHgt
//        }
//       //
//        print("hght----tbl--\(self.tableViewHght.constant)")
        return row
    }
    
    
   
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        tableView.estimatedRowHeight = UITableView.automaticDimension
    }
    
 //   override func layoutSubviews() {
    //    super.layoutSubviews()
        
     //   self.hgtTblView.constant = self.tblView.contentSize.height
 //       //print("self.hgtTblView.constant----\(self.hgtTblView.constant)")
 //   }

   
    func loadViews(){
        self.optionRows = [BPageElementOptionRow]()
        for i in 0...(pageElement?.pageElementOptions?.count)! - 1{
            let indexPath = IndexPath(row: i, section: 0)
            let item = getView(pageElement!.pageElementOptions![i], indexPath: indexPath)
            self.optionRows.append(item)
        }
    }
    
    
    
    
    
    
//    func getSize() -> CGSize?{
//
//        if self.size == nil{
//            if let elements = self.pageElement?.pageElementOptions{
//
//                let count = pageElement?.pageElementOptions?.count
//                var height = CGFloat(0.0)
//                for e in 0...(elements.count - 1){
//
//                    let cell = self.optionRows[e]
//
//                    let newSize = UIUtils.getLabelSizeFor(cell.blabel.text!, labelSize: cell.blabel.frame)
//                    let hg = newSize.height
//                    height = hg * CGFloat(count!)
//                    print(height)
//                }
//                self.size = CGSize(width: self.frame.width, height: height)
//            }
//        }
//        return CGSize(width: self.frame.width, height: self.tableView.contentSize.height)
//    }
    
//    func heightForView(text:String, font:UIFont, width:CGFloat, label: UILabel) -> CGFloat{
//
//        label.frame.size = CGSize(width: width, height: 40)
//        label.numberOfLines = 0
//        label.lineBreakMode = NSLineBreakMode.byWordWrapping
//        label.font = font
//        label.text = text
//        return label.frame.height
//    }
//
    func getView(_ peo: PageElementOption, indexPath: IndexPath) -> BPageElementOptionRow{
        let item = tblView.dequeueReusableCell(withIdentifier: "BPageElementOptionRow") as! BPageElementOptionRow
        
        var checked: String!
        var unchecked: String!
        
        switch pageElement!.QuestionType {
        case .Checkbox:
            checked = "fa-check-square-o_25"
            unchecked = "fa-square-o_25"
        case .Radio:
            checked = "fa-dot-circle-o_25"
            unchecked = "fa-circle-thin_25"
        default:
            checked = "fa-dot-circle-o_25"
            unchecked = "fa-circle-thin_25"
        }
        
        let checkedImage = UIImage(named: checked)!
        let uncheckedImage = UIImage(named: unchecked)!
        
        item.initOptionRow(checkedImage: checkedImage, uncheckedImage: uncheckedImage, watchAnswer: self.watchRightAnswers)
        item.blabel.text = peo.optionValue
        item.blabel.layer.masksToBounds = true
        item.pageElementOption = peo
        
        
           // hg = newSize.height
        item.blabelVw.layer.cornerRadius = 21
       
        switch pageElement!.QuestionType {
        case .Checkbox:
            //item.blabel.layer.cornerRadius = 21
            
            item.blabelLeading.constant = 60
        case .Radio:
            //item.blabel.layer.cornerRadius = 21
            //item.blabelVw.layer.cornerRadius = 21
            item.blabelLeading.constant = 30
            item.optionButton.setImage(nil, for: .normal)
            break
        default:
            break
        }
        
       
        item.optionButton.addTarget(self, action: #selector(optionPressed), for: .touchUpInside)
        return item
    }
    
    @objc func optionPressed(_ sender: UIButton!){
        
        
    
        let button = sender as! BOptionButton
        let optionRow = button.optionRow
        if let hk = self.hideKeyboard {
            hk()
        }
        if (self.pageElement?.QuestionType == PageElement.BType.Checkbox){
   //Apoorva
            if (sender.image(for: .normal) == UIImage.init(named: "fa-square-o_25"))
            {
                isTapOnRow = "true"
                optionRow?.optionButton.setImage(UIImage(named: "fa-check-square-o_25"), for: .normal)
                optionRow?.blabel.backgroundColor = UIColor.clear
                optionRow?.blabelVw.backgroundColor = GlobalBgColor?.darker(by: 13)
                optionRow?.blabel.textColor = UIColor.white
            } else {
                isTapOnRow = ""
                optionRow?.optionButton.setImage(UIImage(named: "fa-square-o_25"), for: .normal)
                optionRow?.blabelVw.backgroundColor = UIColor.white //UIColor.init(hexString: "E8E2E6")
                optionRow?.blabel.backgroundColor = UIColor.clear
                optionRow?.blabel.textColor = UIColor.black
            }
            optionRow?.toggle()
            return
        }
        
        if (self.pageElement?.QuestionType == PageElement.BType.Radio) {
            //Apoorva
            isTapOnRowRadioBtn = "true"
            for or in self.optionRows {
                or.uncheck()
               // isTapOnRow = ""
                print(or)
                or.blabelVw.backgroundColor = UIColor.white //UIColor.init(hexString: "E8E2E6")
                or.blabel.backgroundColor = UIColor.clear
                or.blabel.textColor = UIColor.black
            }
            optionRow?.check()
            optionRow?.blabelVw.backgroundColor = GlobalBgColor?.darker(by: 13)
            optionRow?.blabel.backgroundColor = UIColor.clear
            optionRow?.blabel.textColor = UIColor.white
        }
    }
    
    override func completePage() {
        for optionRow in self.optionRows{
            if optionRow.pageElementOption.answerSelected == true{
                self.pageElement.isAnswered = true
            }
        }
    }
    
    override func loadDefault(){
        for or in optionRows{
            or.loadDefault()
        }
    }
}


class newTableView: UITableView {
    override var intrinsicContentSize: CGSize {
        self.layoutIfNeeded()
        print("content size---\(self.contentSize)")
        return self.contentSize
    }

    override var contentSize: CGSize {
        didSet{

            self.invalidateIntrinsicContentSize()
        }
    }

    override func reloadData() {
        super.reloadData()
        self.invalidateIntrinsicContentSize()
    }
}
