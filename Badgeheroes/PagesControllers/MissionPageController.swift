//
//  MissionPageController.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 5/27/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import UIKit

protocol MissionPageController{
    var missionPage: MissionPage! {get set}
    var pageView: UIView! {get set}
    var pagePosition: Int! {get set}
    var missionId: Int! {get set}
    var watchRightAnswers: Bool! {get set}
    var watchUserAnswers: Bool! {get set}
    
    func completeMissionPage() -> MissionPage
    func isPageCompleted() -> Bool
    func isPageCorrect() -> Bool
    
    func showErrorMessage() -> Void
    
    func loadDefault() -> Void
    
    
}
