//
//  ImageViewController.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 5/26/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import UIKit

class ImageViewController:BasePageViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let imageUrl = self.missionPage.mediaUrl{
            
            self.imageView.kf.setImage(with: URL(string: imageUrl)!)
            //self.view.layoutIfNeeded()
            //self.imageView.layoutIfNeeded()
        }
    }
    
    @IBAction func imageTapped(_ sender: UITapGestureRecognizer) {
        let fullscreenController = self.storyboard!.instantiateViewController(withIdentifier: "FullscreenImageViewController") as! FullscreenImageViewController
        
        fullscreenController.image = self.imageView.image
        fullscreenController.modalFinished = {
            self.view.layoutIfNeeded()
            self.dismiss(animated: false, completion: nil)
        }
        self.present(fullscreenController, animated: false, completion: nil)
    }
    
    func scrollViewWillBeginZooming(_ scrollView: UIScrollView, with view: UIView?) {
        
    }
}
