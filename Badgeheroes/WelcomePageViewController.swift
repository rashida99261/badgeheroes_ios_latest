//
//  WelcomePageViewController.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 5/25/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import UIKit

class WelcomePageViewController: UIPageViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate{

    var pageViews = [UIViewController]()
    var index = 0
    var modalFinished: (() -> Void)?
    
    let numPages = 4
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let pageControl = UIPageControl.appearance()
        
        pageControl.backgroundColor = BColor.IntroBackgroundColor
        pageControl.currentPageIndicatorTintColor = BColor.Light
        pageControl.pageIndicatorTintColor = UIColor.gray
        
        var pages = [UIViewController]()
        let logos = [UIImage.init(named: "intro_app_logo"), UIImage.init(named: "intro_rocket_logo"), UIImage.init(named: "intro_badge_logo"), UIImage.init(named: "intro_cup_logo")]
        let texts = ["Vive una nueva manera de trabajar",
                     "¡Aquí transformamos tu rutina laboral en una experiencia altamente dínamica, entretenida y desafiante!",
                     "BadgeHeroes pondrá a prueba tus habilidades y competencias. ¿Estás preparado para el reto?",
                     "¡Supera cada desafío de manera individual o junto a tu equipo y demuestra que puedes ganar!"]
        
        
        
        for i in 0...(numPages-1){
            let page = self.storyboard!.instantiateViewController(withIdentifier: "IntroPageViewController") as! IntroPageViewController
            page.view.tag = i
            page.view.backgroundColor = BColor.IntroBackgroundColor
            
            page.logoImage.image = logos[i]
            page.descriptionLabel.text = texts[i]
            page.descriptionLabel.sizeToFit()
            pages.append(page)
        }
        
        let page = self.storyboard!.instantiateViewController(withIdentifier: "IntroPageViewController") as! IntroPageViewController
        page.view.tag = numPages
        page.view.backgroundColor = BColor.IntroBackgroundColor
        
        page.logoImage.image = nil
        page.descriptionLabel.text = ""
        page.descriptionLabel.sizeToFit()
        pages.append(page)

        self.pageViews = pages
        self.dataSource = self
        self.delegate = self
    
        self.setViewControllers([pageViews[0]], direction: UIPageViewController.NavigationDirection.forward, animated: false, completion:  nil)
    }
    
    override var shouldAutorotate : Bool {
        return false
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.portrait
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        var index = viewController.view.tag
        
        index -= 1;
        
        if index < 0 {
            return nil
        }
        self.index = index
        return pageViews[index]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        var index = viewController.view.tag
        
        index += 1;
        
        if index > numPages{
            
            return nil
        }
        self.index = index
        return pageViews[index]
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int{
        
        return pageViews.count - 1
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return self.index
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let controller = segue.destination as? LoginContainerViewController{
            controller.modalFinished = self.modalFinished
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool){
        print(pageViewController.presentedViewController?.view.tag as Any)
        
        if self.index == 4 {
            self.performSegue(withIdentifier: "WelcomeToLoginSegue", sender: self)
        }
    }
}
