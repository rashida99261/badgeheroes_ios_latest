//
//  Settings.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 5/25/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import SwiftyUserDefaults
import FirebaseAuth


class Settings{
    static var IsLoggedIn: Bool{
        get{
            return Defaults[.userId] != 0 && Defaults[.accessToken] != ""
        }
    }
    
    static var UserId: Int{
        get{
            return Defaults[.userId]
        }
        
        set(value){
            Defaults[.userId] = value
        }
    }
    
    static var TeamId: Int{
        get{
            if Defaults[.teamId] != 0{
                return Defaults[.teamId]
            }
            return -1
        }
        
        set(value){
            Defaults[.teamId] = value
        }
    }
    
    static var ClientId: Int{
        get{
            if Defaults[.clientId] != 0 {
                return Defaults[.clientId]
            }
            return -1
        }
        
        set(value){
            Defaults[.clientId] = value
        }
    }
    
    static var IsWelcomeDone: Bool {
        get{
            return Defaults[.firstTime]
        }
        set(value){
            Defaults[.firstTime] = value
        }
    }
    
    static var UserName: String {
        get{
            return Defaults[.userName]
        }
        set(value){
            Defaults[.userName] = value
        }
    }
    static var FirstName: String {
        get{
            return Defaults[.firstName]
        }
        set(value){
            Defaults[.firstName] = value
        }
    }
    
    static var LastName: String {
        get{
            return Defaults[.lastName]
        }
        set(value){
            Defaults[.lastName] = value
        }
    }
    
    static var FullName: String {
        get{
            return "\(Defaults[.firstName]) \(Defaults[.lastName])"
        }
    }
    
    static var ImageUrl: String {
        get{
            return Defaults[.imageUrl]
        }
        set(value){
            Defaults[.imageUrl] = value
        }
    }
    
    static var Email: String {
        get{
            return Defaults[.email]
        }
        set(value){
            Defaults[.email] = value
        }
    }
    
    static var AccessToken: String{
        get{
            return Defaults[.accessToken]
        }
        set(value){
            Defaults[.accessToken] = value
        }
    }
    
    static var FirebaseAccessToken: String{
        get{
            return Defaults[.firebaseAccessToken]
        }
        set(value){
            Defaults[.firebaseAccessToken] = value
        }
    }
    
    
    
    
    static var PrimaryColor: String{
        
        get{
            return Defaults[.primaryColor]
        }
        set(value){
            Defaults[.primaryColor] = value
        }
    }
    static var SecondaryColor: String{
        get{
            return Defaults[.secondaryColor]
        }
        set(value){
            Defaults[.secondaryColor] = value
        }
    }
    static var ElementColor: String{
        get{
            return Defaults[.elementColor]
        }
        set(value){
            Defaults[.elementColor] = value
        }
    }
    static var ElementSecondaryColor: String{
        get{
            return Defaults[.elementSecondaryColor]
        }
        set(value){
            Defaults[.elementSecondaryColor] = value
        }
    }
    static var ElementTextColor: String{
        get{
            return Defaults[.elementTextColor]
        }
        set(value){
            Defaults[.elementTextColor] = value
        }
    }
    static var ElementSecondaryTextColor: String{
        get{
            return Defaults[.elementSecondaryTextColor]
        }
        set(value){
            Defaults[.elementSecondaryTextColor] = value
        }
    }
    static var Logo: String{
        get{
            return Defaults[.logo]
        }
        set(value){
            Defaults[.logo] = value
        }
    }
    
    static var UserUpdatedAt: String{
        get{
            return Defaults[.userUpdatedAt]
        }
        
        set(value){
            Defaults[.userUpdatedAt] = value
        }
    }
    
    static var FcmToken: String{
        get{
            return Defaults[.fcmToken]
        }
        
        set(value){
            Defaults[.fcmToken] = value
        }
    }
    
    
    
    static var HelloMessageDone: Bool{
        get{
            return Defaults[.helloMessageDone]
        }
        
        set(value){
            Defaults[.helloMessageDone] = value
        }
    }
    
    static var OS: String{
        get{
            return "ios"
        }
    }
    
    static var DeviceId: String{
        get{
            let ret = UIDevice.current.identifierForVendor!.uuidString
            return ret
        }
        
    }
    
    static var AppVersion: String{
        get{
            if let text = Bundle.main.infoDictionary?["CFBundleVersion"] as? String {
                return text
            }
            return "Not Found"
            
        }
        
    }
    
    
    static var IsStagingMode: Bool{
        get{
            return Defaults[.isStagingMode]
        }
        
        set(value){
            Defaults[.isStagingMode] = value
        }
    }

    static func setColors(_ config: BConfig){
        if let val = config.primaryColor{
            Settings.PrimaryColor = val
        }
        
        if let val = config.secondaryColor{
            Settings.SecondaryColor = val
        }
        
        if let val = config.elementColor{
            Settings.ElementColor = val
        }
        
        if let val = config.elementSecondaryColor{
            Settings.ElementSecondaryColor = val
        }
        
        if let val = config.elementTextColor{
            Settings.ElementTextColor = val
        }
        
        if let val = config.elementSecondaryTextColor{
            Settings.ElementSecondaryTextColor = val
        }
    }
    
    static func logout(){
        self.AccessToken = ""
        self.Email = ""
        self.ImageUrl = ""
        self.LastName = ""
        self.FirstName = ""
        self.UserName = ""
        self.UserId = 0
        self.ElementColor = ""
        self.ElementSecondaryColor = ""
        self.ElementSecondaryTextColor = ""
        self.ElementTextColor = ""
        self.PrimaryColor = ""
        self.SecondaryColor = ""
        self.Logo = ""
        self.IsStagingMode = false
        self.HelloMessageDone = false
        
    }

}
