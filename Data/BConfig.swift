//
//  BConfig.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 6/6/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import ObjectMapper

class BConfig: Mappable{
    var primaryColor: String?
    var secondaryColor: String?
    var elementColor: String?
    var elementSecondaryColor: String?
    var elementTextColor: String?
    var elementSecondaryTextColor: String?
    var x1: String?
    var x2: String?
    var x3: String?
    var updatedAt: String?
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        primaryColor <- map["primary_color"]
        secondaryColor <- map["secondary_color"]
        elementColor <- map["element_color"]
        elementSecondaryColor <- map["element_secondary_color"]
        elementTextColor <- map["element_text_color"]
        elementSecondaryTextColor <- map["element_secondary_text_color"]
        x1 <- map["x1"]
        x2 <- map["x2"]
        x3 <- map["x3"]
        updatedAt <- map["updated_at"]
    }
    
    
}
