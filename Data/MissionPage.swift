//
//  MissionPage.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 5/20/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import ObjectMapper

class MissionPage: Mappable {
    
    /*
 ['form','Formulario'],
 ['photo','Sacar Foto'],
 ['file','Subir Archivo'],
 ['checkin','Geolocalización'],
 ['iframe','Página Web'],
 ['recognition','Reconocimiento'],
 ['message', 'Message'],
 ['image', 'Imagen'],
 ['video','Video']
 */
    enum BType : String{
        case Form = "form"
        case Photo  = "photo"
        case File = "file"
        case Checkin = "checkin"
        case Iframe = "iframe"
        case Recognition = "recognition"
        case Message = "message"
        case Image = "image"
        case Video = "video"
        case NotFound = "not_found"
        
    }
    
    //vienen o se envian al server
    var id: Int?
    var lat: Double?
    var imageUrl: String?
    var fileUrl: String?
    var mediaUrl: String?
    var iframeUrl: String?
    var information: String?
    var lon: Double?
    var recognitionText: String?
    var pageType: String?
    var pageElements: [PageElement] = [PageElement]()
    var recognitionUser: User?
    
    //se procesa una vez recibida la respuesta del servidor
    var userAnswer: UserAnswer?
    
    //Se llenan en os forms
    var isCompleted: Bool = false
    var attempts = 1
    
    
    var PageType: BType{
        get{
            if let pageType = self.pageType{
                if let enumType = BType.init(rawValue: pageType){
                    return enumType
                }
            }
            return BType.NotFound
        }
    }
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map){
        id  <- map["id"]
        lat   <- map["lat"]
        imageUrl  <- map["image_url"]
        fileUrl   <- map["file_url"]
        information   <- map["information"]
        lon   <- map["lon"]
        recognitionText   <- map["recognition_text"]
        pageType  <- map["page_type"]
        pageElements  <- map["page_elements"]
        recognitionUser   <- map["user"]
        mediaUrl <- map["media_url"]
        iframeUrl <- map["iframe_url"]
    }

}
