//
//  PageElementOption.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 5/20/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import ObjectMapper

class PageElementOption: Mappable, Equatable, CustomStringConvertible {

    var id: Int?
    var optionValue: String?
    var answerSelected: Bool?
    var userCounter: Int = 0
    var attempts: [Int] = []
    var isCorrectAnswer: Bool?
    
    //se procesa una vez recibida la respuesta del servidor
    var userAnswer: Bool = false
    
    var AnswerSelected: Bool{
        get{
            if let ansS = answerSelected{
                return ansS
            }else{
                return false
            }
        }
        
    }
    
    var IsCorrectAnswer: Bool{
        get{
            if let isCorrect = isCorrectAnswer{
                return isCorrect
            }else{
                return false
            }
        }
        
        
    }
    
    required init?( map: Map){
    }
    
    
    func mapping(map: Map){
        id <- map["id"]
        optionValue <- map["option_value"]
        answerSelected <- map["selected"]
        userCounter <- map["user_counter"]
        isCorrectAnswer <- map["is_correct_answer"]
        attempts <- map["attempts"]
    }
    
    //es como el toString
    var description: String {
        get {
            if let ov = self.optionValue{
                return ov
            }else{
                return ""
            }
            
        }
    }
    
    

}

func == (lhs: PageElementOption, rhs: PageElementOption) -> Bool{
    
    if let lid = lhs.id, let rid = rhs.id{
        return lid == rid
    }
    return false
}
