//
//  CampaignsResponse.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 6/6/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import Foundation

import ObjectMapper

class CampaignsResponse: Mappable {
    
    var campaigns: [Campaign]?
    var categories: [CampaignCategory]?
    var campaignTree: CampaignTree?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map){
        campaigns <- map["campaigns"]
        categories <- map["categories"]
        campaignTree <- map["campaign_tree"]
    }
    
}
