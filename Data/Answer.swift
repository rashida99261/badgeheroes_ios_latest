//
//  Answer.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 5/20/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import ObjectMapper

class Answer: Mappable {
    
    
    enum AnswerState : Int{
        case Finished = 2
        case Accepted = 3
        case Rejected = 6
        case NotFound = 0
    }
    
    var id: Int?
    var state: Int?
    var comment: String?
    var createdAt: Date?
    
    required init?(map: Map){
        
    }
    
    
    var State: AnswerState{
        get{
            if let state = self.state{
                if let enumState = AnswerState.init(rawValue: state){
                    return enumState
                }
            }
            return AnswerState.NotFound
        }
    }
    
    var CreatedAt: String{
        if let ca = self.createdAt{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "'Finalizada el 'dd-MM-yyyy' a las 'hh:mm"
            
            let ret = dateFormatter.string(from: ca)
            return ret
        }
        return ""
    }
    func mapping(map: Map){
        id <- map["id"]
        state <- map["state"]
        comment <- map["comment"]
        createdAt <- (map["created_at"], DateParser())
        
    }
    
}
