//
//  Team.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 5/20/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import ObjectMapper

class Team: Mappable {

    var id: Int?
    var name: String?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map){
        id <- map["id"]
        name <- map["name"]
    }

}
