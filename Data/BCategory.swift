//
//  BCategory.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 7/6/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import Foundation

protocol BCategory {
    var id: Int? {get}
    var name: String? {get}
}