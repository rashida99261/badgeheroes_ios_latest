//
//  Mission.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 5/20/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import ObjectMapper

open class Mission: Mappable, Loggable{

    //enum state: [:not_started,:started, :finished, :accepted]
    public enum MissionState : Int{
        case noStarted = 0
        case started   = 1
        case finished  = 2
        case accepted  = 3
        case locked    = 4
        case expired   = 5
        case dailyLimit = 7
        case maxLimit = 8
        case notFound  = -1
    }
    
    public enum MissionTarget : Int{
        case elearning = 0
        case quiz   = 1
        
    }
    
    
    var id: Int?
    var credits: Int?
    var endDate: Date?
    var experience: Int?
    var name: String?
    var imageUrl: String?
    var description: String?
    var shortDescription: String?
    var needsRevision: Bool?
    var missionPages: [MissionPage]?
    var state: Int?
    var dependants: [Mission]?
    var tags: String?
    var totalDoneRepeats: Int?
    var totalMaxRepeats: Int?
    var dailyDoneRepeats: Int?
    var dailyMaxRepeats: Int?
    var finishedAnswers: Int?
    var acceptedAnswers: Int?
    var rejectedAnswers: Int?
    var repeatable: Bool?
    var target: Int?
    var answerId: Int?
    var userAnswers: [UserAnswer]?
    
    
    
    var State: MissionState{
        get{
            if let state = self.state{
                if let enumState = MissionState.init(rawValue: state){
                    return enumState
                }
            }
            return MissionState.notFound
        }
    }
    
    var Target: MissionTarget{
        get{
            if let target = self.target{
                if let enumTarget = MissionTarget.init(rawValue: target){
                    return enumTarget
                }
            }
            return MissionTarget.elearning
        }
    }
    
    var Tags: [String]{
        get{
            if let ts = tags{
                return ts.components(separatedBy: ",")
            }
            return [String]()
        }
    }
    
    var Repeatable: Bool{
        get{
            if let r = self.repeatable{
                return r
            }
            return false
        }
    }
    
    
    var Grade: Int{
        get{
            if self.Target == .quiz{
                var correct = 0
                var total = 0
                for mp in self.missionPages!{
                    if mp.PageType == .Form{
                        for pe in mp.pageElements{
                            if let hasAnswer = pe.hasAnswer, hasAnswer == true{
                                total += 1
                                if (pe.isCorrectAndSaveAnswers(attempt: 1)){
                                    correct += 1
                                }
                                
                            }
                            
                        }
                    }
                }
                if total > 0{
                    return Int((Double(correct)*1.0/Double(total))*100)
                }else{
                    return 100
                }
                
            }else{
                return 100
            }
            
        }
    }
    
    var AnswerId: Int{
        get{
            if let ai = answerId{
                return ai
            }
            return -100
        }
    }
    
    required public init?(map: Map){
        
    }
    
    open func mapping(map: Map){
        id  <- map["id"]
        credits <- map["credits"]
        endDate <- (map["end_date"], DateParser())
        experience  <- map["experience"]
        name  <- map["name"]
        imageUrl  <- map["image_url"]
        description <- map["description"]
        shortDescription <- map["short_description"]
        needsRevision <- map["needs_revision"]
        missionPages  <- map["mission_pages"]
        state <- map["state"]
        dependants <- map["dependants"]
        tags <- map["tags"]
        totalDoneRepeats <- map["total_done_repeats"]
        totalMaxRepeats <- map["total_max_repeats"]
        dailyDoneRepeats <- map["daily_done_repeats"]
        dailyMaxRepeats <- map["daily_max_repeats"]
        finishedAnswers <- map["finished_answers_count"]
        acceptedAnswers <- map["accepted_answers_count"]
        rejectedAnswers <- map["rejected_answers_count"]
        repeatable <- map["repeatable"]
        target <- map["target"]
        answerId <- map["answer_id"]
        userAnswers <- map["answers"]
    }
    
    func available() -> Bool{
        if self.State == MissionState.noStarted || self.State == MissionState.started{
            return true
        }
        return false
    }
    
    func logCompleted(_ me: Bool) -> String{
        if(me){
            return "Haz completado con éxito la misión \"\(name!)\"";
        }else{
            return " ha completado con éxito la misión \"\(name!)\"";
        }
    }
    
    func getMissionPageAnswers(missionPageId: Int) -> UserAnswer?{
        if let userAnswers = self.userAnswers {
            for userAnswer : UserAnswer in userAnswers {
                if userAnswer.MissionPageId == missionPageId{
                    return userAnswer
                }
            }
        }
        return nil
    }
    
    func getPageElementAnswer(pageElementId: Int) -> UserAnswer?{
        if let userAnswers = self.userAnswers {
            for userAnswer : UserAnswer in userAnswers {
                if userAnswer.PageElementId == pageElementId{
                    return userAnswer
                }
            }
        }
        return nil
    }
    
    func isPageElementOptionSelected(pageElementOptionId: Int) -> Bool{
        if let userAnswers = self.userAnswers {
            for userAnswer : UserAnswer in userAnswers {
                if userAnswer.PageElementOptionSelectedId == pageElementOptionId{
                    return true
                }
            }
        }
        return false
    }
    
    func assignAnswers(){
        if let _ = self.userAnswers, let missionPages = self.missionPages {
            for missionPage in missionPages {
                let answer = getMissionPageAnswers(missionPageId: missionPage.id!)
                
                missionPage.lat = answer?.lat
                missionPage.lon = answer?.lon
                missionPage.fileUrl = answer?.fileUrl
                
                for pe in missionPage.pageElements{
                    let peAnswer = getPageElementAnswer(pageElementId: pe.id!)
                    
                    pe.answerText = peAnswer?.answerText
                    
                    if let peos = pe.pageElementOptions{
                        for peo in peos{
                            peo.answerSelected = isPageElementOptionSelected(pageElementOptionId: peo.id!)
                        }
                    }
                }
            }
        }
    }
    
    func logLevelUp(_ me: Bool) -> String{
        if(me){
            return "Haz completado con éxito la misión \"\(name!)\"";
        }else{
            return " ha completado con éxito la misión \"\(name!)\"";
        }
    }
    
    func logRejected(_ me: Bool) -> String{
        if(me){
            return "La misión \"\(name!)\" fue rechazada";
        }else{
            return "La misión \"\(name!)\" fue rechazada";
        }
    }
    
    
    func logGetContent(_ state: HistoryLog.LogState, me: Bool) -> String{
        switch (state) {
        case .Completed:
            return logCompleted(me);
        case .LevelUp:
            return logLevelUp(me);
        case .Rejected:
            return logRejected(me);
        default:
            return "";
        }
    }

}
