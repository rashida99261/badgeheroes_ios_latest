//
//  UserAnswer.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 8/31/18.
//  Copyright © 2018 MisPistachos. All rights reserved.
//

import ObjectMapper

class UserAnswer: Mappable {
    
    var id: Int?
    var answerNumber: Int?
    var checkinDate: Date?
    var lat: Double?
    var lon: Double?
    var missionPageId: Int?
    var pageElementId: Int?
    var fileUrl: String?
    var pageElementOptionSelectedId: Int?
    var answerText: String?
    
    var MissionPageId: Int{
        get{
            if let missionPageId = self.missionPageId{
                return missionPageId
            }
            return -1
        }
    }
    
    var PageElementId: Int{
        get{
            if let pageElementId = self.pageElementId{
                return pageElementId
            }
            return -1
        }
    }
    
    var PageElementOptionSelectedId: Int{
        get{
            if let pageElementOptionSelectedId = self.pageElementOptionSelectedId{
                return pageElementOptionSelectedId
            }
            return -1
        }
    }
    
    required init?(map: Map){
        
    }

    func mapping(map: Map){
        id <- map["id"]
        answerNumber <- map["answer_number"]
        checkinDate <- (map["checkin_date"], DateParser())
        lat <- map["lat"]
        lon <- map["lon"]
        fileUrl <- map["file_url"]
        missionPageId <- map["mission_page_id"]
        pageElementId <- map["page_element_id"]
        answerText <- map["answer_text"]
        pageElementOptionSelectedId <- map["page_element_option_selected_id"]
        
    }
    
}
