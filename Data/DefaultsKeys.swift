//
//  Settings.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 5/20/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//


import SwiftyUserDefaults

//Equivalente a Shared preferences. Para Usarlas, hay que llamar a Defaults[.userId]
extension DefaultsKeys {
    
    static let userId = DefaultsKey<Int>("userId")
    static let teamId = DefaultsKey<Int>("teamId")
    static let clientId = DefaultsKey<Int>("clientId")
    
    static let userName = DefaultsKey<String>("userName")
    static let firstName = DefaultsKey<String>("firstName")
    static let lastName = DefaultsKey<String>("lastName")
    static let accessToken = DefaultsKey<String>("accessToken")
    static let firebaseAccessToken = DefaultsKey<String>("firebaseAccessToken")
    static let imageUrl = DefaultsKey<String>("imageUrl")
    static let email = DefaultsKey<String>("email")
    static let firstTime = DefaultsKey<Bool>("firstTime")
    static let userUpdatedAt = DefaultsKey<String>("updatedAt")
    
    //Color de barra principal
    static let primaryColor = DefaultsKey<String>("primaryColor")
    //Color de barra de edit user
    static let secondaryColor = DefaultsKey<String>("secondaryColor")
    //Color de botones/textos/cosas clickeadas por defecto
    static let elementColor = DefaultsKey<String>("elementColor")
    //Color de botones alternativos (clickeados, rechazar, etc)
    static let elementSecondaryColor = DefaultsKey<String>("elementSecondaryColor")
    //Color de texto de los botones
    static let elementTextColor = DefaultsKey<String>("elementTextColor")
    //Color de texto de los botones en estado secundario
    static let elementSecondaryTextColor = DefaultsKey<String>("elementSecondaryTextColor")
    
    
    
    static let logo = DefaultsKey<String>("logo")
    
    static let fcmToken = DefaultsKey<String>("fcmToken")
    
    static let helloMessageDone = DefaultsKey<Bool>("helloMessageDone")
    static let isStagingMode = DefaultsKey<Bool>("isStagingMode")
    
    


}
