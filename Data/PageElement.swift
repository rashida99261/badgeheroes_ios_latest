//
//  PageElement.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 5/20/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import ObjectMapper

class PageElement: Mappable {

    var id: Int?
    var label: String?
    var answerText: String?
    var questionType: String?
    var position: Int?
    var isRequired: Bool?
    
    //si está respondido por el usuario
    var isAnswered: Bool?
    
    //si tiene respuesta correcta
    var hasAnswer: Bool?
    var pageElementOptions: [PageElementOption]?
    
    //si tiene media adjunta
    var mediaUrl: String?
    
    var retries: Int?
    
    //se procesa una vez recibida la respuesta del servidor
    var userAnswer: UserAnswer?
    
    //enum page_type: [:not_started,:started, :finished, :accepted]
    enum BType : String{
        case Input =    "input"
        case TextArea   = "textarea"
        case Image      = "image"
        case Video      = "video"
        case Select     = "select"
        case Radio      = "radio"
        case Checkbox   = "checkbox"
        case Message    = "message"
        case NotFound   = ""
    }
    
    var HasAnswer: Bool{
        if let ha = self.hasAnswer{
            return ha
        }
        return false
    }
    
    
    
    var QuestionType: BType{
        get{
            if let questionType = self.questionType{
                if let enumType = BType.init(rawValue: questionType){
                    return enumType
                }
            }
            return BType.NotFound
        }
    }
    
    
    var Options: [[String]]{
        get{
            var ret = [[String]]()
            if let peos = pageElementOptions{
                peos.forEach({ (peo: PageElementOption) in
                    let val = ["\(peo.id!)", peo.optionValue!]
                    ret.append(val)
                })
                
            }
            return ret
        }
        
      
    }
    
    var SelectedOption: PageElementOption?{
        if let peos = self.pageElementOptions{
            for peo in peos{
                if let selected = peo.answerSelected{
                    if selected == true{
                        return peo
                    }
                }
            }
        }
        return nil
    }
    
    var Retries: Int{
        get{
            if let r = self.retries{
                return r
            }else{
                return 0
            }
        }
        
        set(value){
            self.retries = value
        }
    }
    
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map){
        id  <- map["id"]
        label <- map["label"]
        answerText  <- map["answer_text"]
        questionType  <- map["question_type"]
        position  <- map["position"]
        isRequired  <- map["required"]
        pageElementOptions  <- map["page_element_options"]
        hasAnswer <- map["has_answer"]
        mediaUrl <- map["media_url"]
        retries <- map["retries"]
    }
    
    
    func isCorrectAndSaveAnswers(attempt: Int) -> Bool{
        var ret = true
        
        if self.HasAnswer{
            if let peos = self.pageElementOptions , peos.count > 0{
                if self.QuestionType == PageElement.BType.Checkbox{
                    //Checkbox, todas las marcadas como correctas deben ser seleccionadas
                    
                   
                    
                    for peo in peos{
                        
                        print(peo)
                        let isCorrect = peo.IsCorrectAnswer
                        let isChecked = peo.AnswerSelected
                        //let isChecked = isCheckedRow
                        if isChecked {
                            peo.userCounter += 1
                            peo.attempts.append(attempt)
                        }
//                        print("attemptattemptattemptattemptattemptattempt")
//                        print(peo.attempts)
                        
                        if isCorrect == true{
                            //si la correcta no está marcada
                            if isChecked == false{
                                self.Retries = self.Retries + 1
                                ret = false
                            }
                            else{
                                print("correct ans choose")
                            }
                            
                        }else{
                            //si esta marcada una no correcta
                            if isChecked == true{
                                self.Retries = self.Retries + 1
                                ret = false
                            }
                        }
                    }
                    
                }else{
                    //Radio Button o Lista. Basta con que uno esté seleccionado
                    
                    
                    for peo in peos{
                        
                        print(peo)
                        let isCorrect = peo.IsCorrectAnswer
                        let isChecked = peo.AnswerSelected
                        //let isChecked = isCheckedRow
                        if isChecked {
                            peo.userCounter += 1
                            peo.attempts.append(attempt)
                        }
                        //                        print("attemptattemptattemptattemptattemptattempt")
                        //                        print(peo.attempts)
                        
                        if isCorrect == true{
                            //si la correcta no está marcada
                            if isChecked == false{
                                self.Retries = self.Retries + 1
                                ret = false
                            }
                            else{
                                print("correct ans choose")
                            }
                            
                        }else{
                            //si esta marcada una no correcta
                            if isChecked == true{
                                self.Retries = self.Retries + 1
                                ret = false
                            }
                        }
                    }
                    
//                    var rightAnswer = false
//                    for peo in peos{
//                        let isCorrect = peo.IsCorrectAnswer
//                        let isChecked = peo.AnswerSelected
//                        if isChecked {
//                            peo.userCounter += 1
//                            peo.attempts.append(attempt)
//                        }
//
//
//                        if isCorrect == true && isChecked == true{
//                            rightAnswer = true
//                            break
//                        }
//                    }
//                    if !rightAnswer{
//                        self.Retries = self.Retries + 1
//
//                        ret = false
//                    }
                }
            }
            
        }
        print(ret)
        return ret

    }

}
