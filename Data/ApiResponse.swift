//
//  ApiResponse.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 5/20/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import ObjectMapper

class ApiResponse: Mappable{

    
    var status: String?
    var message: String?
    var error: Bool?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map){
        status <- map["status"]
        message <- map["message"]
        error <- map["error"]
    }
}
