//
//  HistoryLog.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 5/20/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import ObjectMapper

class HistoryLog: Mappable {

    enum LogState : String{
        case Completed = "completed"
        case LevelUp  = "levelup"
        case Rejected = "rejected"
        case NotFound = "not_found"
    }
    
    var id: Int?
    var state: String?
    var user: User?
    var createdAt: Date?
    var medal: Medal?
    var mission: Mission?
    var title: Title?
    
    var State: LogState{
        get{
            if let state = self.state{
                if let enumState = LogState.init(rawValue: state){
                    return enumState
                }
            }
            return LogState.NotFound
        }
    }
    
    
    var CreatedAt: String{
        if let ca = self.createdAt{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy' a las 'hh:mm"
            
            let ret = dateFormatter.string(from: ca)
            return ret
        }
        return ""
    }
    
    var Message: String{
        let resource: Loggable?
        
        if let r = self.medal{
            resource = r
        }else if let r = self.mission{
            resource = r
        }else if let r = self.title{
            resource = r
        }else{
            resource = nil
        }
        
        if let userId = self.user?.id{
            var me = false
            var messageUserName = self.user!.FullName
            if userId == Settings.UserId{
                me = true
                messageUserName = ""
            }
            
            if let res = resource{
                return "\(messageUserName) \(res.logGetContent(self.State, me: me))"
            }
        }
        return ""
    }
    
    var ImageUrl: String{
        if let resource = self.medal{
            if let _ = resource.imageUrl
            {
                return resource.imageUrl!
            }
            return ""
        }else if let resource = self.mission{
            if let _ = resource.imageUrl
            {
                return resource.imageUrl!
            }
            return ""
        }else if let resource = self.title{
        //    print("imggggg = \(resource.imageUrl)")
            if let _ = resource.imageUrl
            {
               return resource.imageUrl!
            }
            return ""
        }else{
            return ""
        }
    }
    required init?(map: Map){
        
    }
    
    func mapping(map: Map){
        id <- map["id"]
        state <- map["state"]
        user <- map["user"]
        createdAt <- (map["created_at"], DateParser())
        medal <- map["medal"]
        mission <- map["mission"]
        title <- map["title"]
    }
    
    

}
