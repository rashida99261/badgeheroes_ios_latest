//
//  Medal.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 5/20/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import ObjectMapper

class Medal: Mappable, Loggable {

    
    var id: Int?
    var name: String?
    var credits: Int?
    var description: String?
    var imageUrl: String?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map){
        id          <- map["id"]
        name        <- map["name"]
        credits     <- map["credits"]
        description <- map["description"]
        imageUrl    <- map["image_url"]
    }
    
    func logCompleted(_ me: Bool) -> String{
        if(me){
            return "¡Felicitaciones! te has ganado la medalla \"\(self.name!)\"";
        }else{
            return " ha ganado la medalla \"\(self.name!)\"";
        }
    }
    
    func logLevelUp(_ me: Bool) -> String{
        if(me){
            return "¡Felicitaciones! te has ganado la medalla \"\(self.name!)\"";
        }else{
            return "  ha ganado la medalla \"\(self.name!)\"";
        }
    }
    
    func logRejected(_ me: Bool) -> String{
        if(me){
            return "¡Felicitaciones! te has ganado la medalla \"\(self.name!)\"";
        }else{
            return "  ha ganado la medalla \"\(self.name!)\"";
        }
    }
    
    func logGetContent(_ state: HistoryLog.LogState, me: Bool) -> String{
        switch (state) {
        case .Completed:
            return logCompleted(me);
        case .LevelUp:
            return logLevelUp(me);
        case .Rejected:
            return logRejected(me);
        default:
            return "";
        }
    }

}

class UserRanking: Mappable {
    
    var position: Int?
    var level: String?
    var firstname: String?
    var lastname: String?
    var experience: Int?
    var imageUrl: String?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map){
        position          <- map["position"]
        level        <- map["level"]
        firstname     <- map["first_name"]
        lastname <- map["last_name"]
        experience    <- map["experience"]
        imageUrl    <- map["image"]
    }
    
}
