//
//  repeatedMissionPopUpVC.swift
//  Badgeheroes
//
//  Created by Rashida on 19/08/19.
//  Copyright © 2019 MisPistachos. All rights reserved.
//

import UIKit

class repeatedMissionPopUpVC: UIViewController {
    
    @IBOutlet weak var lblCounts: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    
    var strRepeatedCounts = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.clear
        view.isOpaque = false
        
        let imageAttachment =  NSTextAttachment()
        imageAttachment.image = UIImage(named:"paper_icon")
        //Set bound to reposition
        let imageOffsetY:CGFloat = -5.0;
        imageAttachment.bounds = CGRect(x: 0, y: imageOffsetY, width: 15, height: 15)
        //Create string with attachment
        let attachmentString = NSAttributedString(attachment: imageAttachment)
        //Initialize mutable string
        let completeText = NSMutableAttributedString(string: "")
        //Add image to mutable string
        completeText.append(attachmentString)
        //Add your text to mutable string
        let  textAfterIcon = NSMutableAttributedString(string: " \(strRepeatedCounts)")
        completeText.append(textAfterIcon)
        self.lblCounts.attributedText = completeText
        lblTitle.text = "TE QUEDAN \(strRepeatedCounts) ENVIOS POR HOY"

        // Do any additional setup after loading the view.
    }
    

    @IBAction func clickOOKBtn(_ sender: UIButton){
        self.dismiss(animated: true, completion: nil)
    }

}
