//
//  newFormViewController.swift
//  Badgeheroes
//
//  Created by Reinforce on 21/09/19.
//  Copyright © 2019 MisPistachos. All rights reserved.
//

import UIKit



class newFormViewController: UIViewController, UITableViewDataSource, UITableViewDelegate{

    var missionId: Int!
    var missionPage: MissionPage!
    var elements: [PageElement]!
    var pagePosition: Int!
    var pageView: UIView!
    var watchRightAnswers: Bool! = false
    var watchUserAnswers: Bool! = false
    
    var rows = [Int: BBaseRow]()
    
    
    @IBOutlet weak var tblQue : UITableView!
    
    @IBOutlet weak var viewHeight: UIView!
    @IBOutlet weak var viewConstraint : NSLayoutConstraint!
      @IBOutlet weak var tblHeighnt : NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.elements = missionPage.pageElements
        
        self.rows = [Int: BBaseRow]()
        
        
        if(self.elements.count > 0){
            for e in 0...(self.elements.count-1){
                rows[e] = getCell(e)
            }
        }
        
        
        
//        for _ in 0...(self.elements.count-1){
//            
//            //intRow.append(e)
//            //intRow.append(e)
//        }
        
        self.tblQue.tableFooterView = UIView()
        self.tblQue.reloadData()
        
        //view.layoutIfNeeded()
        
    }
    

    
    override func viewDidAppear(_ animated: Bool) {
    //self.tblHeighnt.constant = self.tblQue.contentSize.height
        
    }
    
    override func viewWillLayoutSubviews() {
    self.tblHeighnt.constant = self.tblQue.contentSize.height
        
    }
    
    func getCell(_ index: Int) -> BBaseRow?{
        let indexPath = IndexPath(row: index, section: 0)
        return getCell(indexPath: indexPath)
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.elements.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        //let row = (indexPath as NSIndexPath).row
        let item: UITableViewCell = getCell(indexPath: indexPath)!
        
        //item.layoutIfNeeded()
       // item.layoutMarginsDidChange()
        //item.reloadInputViews()
        return item
    }
    
   // func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     func getCell(indexPath: IndexPath) -> BBaseRow?{
        
        let index = indexPath.row
        if(index < 0){
            return nil
        }
        if(self.rows[index] != nil){
            print(Array(self.rows.keys))
            return self.rows[index]
        }
        
        
        let pe = self.elements[index]
        var item: BBaseRow?
        
        //let cell = UITableViewCell()
       // let pe = self.elements[indexPath.row]
        
       // var item: BBaseRow?
        
        switch pe.QuestionType {
        case .Message:
            
            let cell = tblQue.dequeueReusableCell(withIdentifier: "messageCell") as! messageCell
            cell.labelmsgView.text = pe.label!.htmlToString
            item = cell
            //return cell
            
        case .Input:
            let row = tblQue.dequeueReusableCell(withIdentifier: "BTextFieldRow") as! BTextFieldRow
            row.pageElement = pe
          // self.tblHeighnt.constant = self.tblQue.contentSize.height
            item = row
           // swati return row
            
        case .TextArea:
            let cell = tblQue.dequeueReusableCell(withIdentifier: "textAreaCell") as! textAreaCell
            cell.pageElement = pe
            cell.textView.delegate = self
            cell.textView.textColor = UIColor.lightGray
          //  self.tblHeighnt.constant = self.tblQue.contentSize.height
            item = cell
           // return cell
            
            
        case .Image:
            let cell = tblQue.dequeueReusableCell(withIdentifier: "BImageRow") as! BImageRow
            cell.pageElement = pe
            cell.imageButton.kf.setImage(with: URL(string: pe.mediaUrl!)!)
            cell.imagePressed = self.showImageModal
            
            item = cell
          //  return cell
            
        case .Video:
            let cell = tblQue.dequeueReusableCell(withIdentifier: "BVideoRow") as! BVideoRow
            cell.pageElement = pe
            cell.controller = self
            cell.loadVideo()
           // self.tblHeighnt.constant = self.tblQue.contentSize.height + 265
            item = cell
           // return cell
        
        case .Select:
            let cell = tblQue.dequeueReusableCell(withIdentifier: "BListSelectionRow") as! BListSelectionRow
            cell.pageElement = pe
            
            cell.buttonPressed = {
                let optionMenu = cell.getSelectOptionDialog()
                self.present(optionMenu, animated: true, completion: nil)
            }
           // self.tblHeighnt.constant = self.tblQue.contentSize.height
            item = cell
          //  return cell
            
        case .Radio, .Checkbox:
            let cell = tblQue.dequeueReusableCell(withIdentifier: "BMultipleSelectRow") as! BMultipleSelectRow
            cell.pageElement = pe
            cell.watchRightAnswers = self.watchRightAnswers
            cell.watchUserAnswers = self.watchUserAnswers
            cell.loadViews()

            item = cell
           // return cell
            
        default:
            print("")
        }
        
        item?.isUserInteractionEnabled = !(self.watchRightAnswers || self.watchUserAnswers)
        self.rows[index] = item
        return item
      //  self.rows[indexPath.row] = item
       // return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        let pe = self.elements[indexPath.row]
        switch pe.QuestionType {
         case .Image:
            self.tblHeighnt.constant = self.tblQue.contentSize.height
            return 250
        default:
            print("")
        }
    
        return self.tblQue.rowHeight
    }
    
    func showImageModal(_ imageView: UIImageView){
        let fullscreenController = self.storyboard!.instantiateViewController(withIdentifier: "FullscreenImageViewController") as! FullscreenImageViewController
        fullscreenController.image = imageView.image
        fullscreenController.modalFinished = {
            self.dismiss(animated: false, completion: nil)
        }
        self.present(fullscreenController, animated: false, completion: nil)
    }
    
}


extension newFormViewController : MissionPageController{
    
    func loadDefault() {
        if(self.elements.count > 0){
            for e in 0...(self.elements.count-1){
                rows[e] = getCell(e)
                rows[e]!.loadDefault()
            }
        }
    }
    
    func isPageCompleted() -> Bool {
        if(watchUserAnswers){
            return true
        }
        
        let pes = self.elements
        if (pes?.count)! > 0{
            for i in 0...((pes?.count)! - 1){
                let pe = pes?[i]
                if let req = pe?.isRequired , req == true{
                    if pe?.isAnswered != true{
                        return false
                    }
                    
                }
            }
        }
        return true
    }
    
    func showErrorMessage(){
        UIUtils.toast("Hay campos requeridos no completados en esta página")
    }
    
    
    func isPageCorrect() -> Bool{
        
        let pes = self.missionPage!.pageElements
        var isCorrect = true
        if pes.count == 0 {
            return true
        }
        for i in 0...(pes.count - 1){
            let pe = pes[i]
            
            
            if let hasAnswer = pe.hasAnswer , hasAnswer == true{
                if !pe.isCorrectAndSaveAnswers(attempt: missionPage.attempts){
                    isCorrect = false
                }
            }
        }
        missionPage.attempts = missionPage.attempts + 1
        return isCorrect
    }
    
    func completeMissionPage() -> MissionPage{
        let listRows = Array(self.rows.values)
        for row in listRows{
            row.completePage()
        }
        return missionPage
    }
}

extension newFormViewController : UITextFieldDelegate, UITextViewDelegate{
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text.isEmpty {
            textView.text = "Escribe tu respuesta aquí"
            textView.textColor = UIColor.black
        }
    }
}


class messageCell: BBaseRow {
    
    @IBOutlet weak var labelmsgView: UILabel!
    @IBOutlet weak var labelmsgContainerView: UIView!
}

class textAreaCell: BBaseRow{
    
    @IBOutlet weak var textView: UITextView!

     override func completePage() {
        let ans = self.textView.text
        if ans != "" && ans != "Escribe tu respuesta aquí"{
            if self.pageElement != nil{
                self.pageElement.answerText = textView.text
                self.pageElement.isAnswered = true
            }
        }
    }
    
    override func loadDefault(){
        if let ans = pageElement.answerText{
            self.textView.text = ans
        }
    }
    
}

extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}

