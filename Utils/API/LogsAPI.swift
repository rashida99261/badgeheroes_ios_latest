//
//  LogsAPI.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 5/24/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import Alamofire
import AlamofireObjectMapper
import ObjectMapper
import Foundation

open class LogsAPI: BaseAPI{
    
    //genera tab
    static func getHistoryLog(userId: Int, callback: @escaping ([HistoryLog]?, NSError?) -> Void){
        let path = "logs/users/\(userId)"
        LogsAPI.get(path , parameters: [String:AnyObject](), callback)
    }
    
    //user tab
    static func getPeopleHistoryLog(lastLogId: Int, callback: @escaping ([HistoryLog]?, NSError?) -> Void){
        let log_id = String(lastLogId) as AnyObject
        LogsAPI.get("logs/people" , parameters: ["last_log_id": log_id], callback)
    }
    
}
