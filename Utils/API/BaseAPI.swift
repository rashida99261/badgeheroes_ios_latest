//
//  BaseAPI.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 5/23/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import Alamofire
import ObjectMapper
import AlamofireObjectMapper

extension Request {
    public func debugLog() -> Self {
        #if DEBUG
            //debugPrint(self)
        #endif
        return self
    }
}

open class BaseAPI{
    
    static let API_URL :String = Environment.API_URL
    
    static var alamofireManager: Alamofire.SessionManager?
    
    public static func request<T: Mappable>(_ method: Alamofire.HTTPMethod, path: String, parameters: [String:AnyObject], _ callback : @escaping (T?, NSError?) -> Void){
        
        getAlamofire(method, path: path, parameters: parameters).responseObject { (response: DataResponse<T>) in
            let object = response.result.value
            let error = response.result.error

            verifyError(error as NSError?)
            
            callback(object, error as NSError?)
        }
    }
    
    public static func request<T: Mappable>(_ method: Alamofire.HTTPMethod, path: String, parameters: [String:AnyObject], _ callback : @escaping ([T]?, NSError?) -> Void){
        
        getAlamofire(method, path: path, parameters: parameters).responseArray { (response: DataResponse<[T]>) in
            let objects = response.result.value
            let error = response.result.error
            verifyError(error as NSError?)
            callback(objects, error as NSError?)
        }
    }
    
    
    public static func request(_ method: Alamofire.HTTPMethod,path: String, parameters: [String:AnyObject], _ callback : @escaping (NSDictionary?, NSError?) -> Void){
        
        getAlamofire(method, path: path, parameters: parameters).responseJSON { (response: DataResponse<Any>)  in
            let objects = response.result.value
            let error = response.result.error
            
            verifyError(error as NSError?)
            
            callback((objects as! NSDictionary), error as NSError?)
        }
    }
    
    
    
    static fileprivate func getAlamofire(_ method: Alamofire.HTTPMethod,path: String, parameters: [String: AnyObject]) -> DataRequest{
        
        if self.alamofireManager == nil {
            let configuration = URLSessionConfiguration.default
            configuration.timeoutIntervalForResource = 15 // seconds
            self.alamofireManager = Alamofire.SessionManager(configuration: configuration)
        }
        
        let token = Settings.AccessToken
        let headers = ["access-token": token, "api-version": String(Environment.API_VERSION)]
        
        
        var url = "\(API_URL)"
        if Settings.IsStagingMode{
            url = "\(Environment.STAGING_API_URL)"
        }
        url = "\(url)\(path)"
        
        var encoding : Alamofire.ParameterEncoding = Alamofire.JSONEncoding.default
        
        if method == Alamofire.HTTPMethod.get {
            encoding = Alamofire.URLEncoding.default
        }
        return self.alamofireManager!.request(url, method: method, parameters: parameters, encoding: encoding, headers: headers)
    }
    
    
    public static func put<T: Mappable>(_ path: String, parameters: [String:AnyObject], _ callback : @escaping (T?, NSError?) -> Void){
        
        self.request(.put, path: path, parameters: parameters, callback)
    }
    
    public static func get<T: Mappable>(_ path: String, parameters: [String:AnyObject], _ callback : @escaping (T?, NSError?) -> Void){
        
        self.request(.get, path: path, parameters: parameters, callback)
    }
    
    public static func get<T: Mappable>(_ path: String, parameters: [String:AnyObject], _ callback : @escaping ([T]?, NSError?) -> Void){
        
        self.request(.get, path: path, parameters: parameters, callback)
    }
    
    public static func post<T: Mappable>(_ path: String, parameters: [String:AnyObject], _ callback : @escaping (T?, NSError?) -> Void){
        
        self.request(.post, path: path, parameters: parameters, callback)
    }
    
    public static func post<T: Mappable>(_ path: String, parameters: [String:AnyObject], _ callback : @escaping ([T]?, NSError?) -> Void){
        
        self.request(.post, path: path, parameters: parameters, callback)
    }
    
    fileprivate static func verifyError(_ error: NSError?){
        if let e = error{
            print("Request failed with error: \(e)")
            UIUtils.runOnUIThread({
                UIUtils.toast("No se pudo conectar con el servidor.\n Verifica tu conexión a internet")
            })
        }
    }
}
