//
//  UsersAPI.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 5/23/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import Alamofire
import AlamofireObjectMapper
import ObjectMapper
import Foundation

open class UsersAPI: BaseAPI{
    
    
    enum CommentType: Int{
        case problem  = 0
        case other    = 1
    }
    
    static func performLogin(email: String, password: String,  callback: @escaping (User?, NSError?) -> Void){
        let email = email as AnyObject 
        let password = password as AnyObject 
        let deviceId = Settings.DeviceId as AnyObject
        let fcmToken = Settings.FcmToken as AnyObject
        let os = Settings.OS as AnyObject
        let appVersion = Settings.AppVersion as AnyObject
        UsersAPI.post("log_in", parameters: ["email": email as AnyObject, "password": password, "device_id": deviceId, "fcm_token": fcmToken, "os": os, "app_version": appVersion], callback)
    }
    
    static func preLogin(email: String, _ callback : @escaping (User?, NSError?) -> Void){
        let email = email as AnyObject
        UsersAPI.get("pre_login", parameters: ["email": email], callback)
    }
    
    static func getUser(userId id: Int, _ callback: @escaping (User?, NSError?) -> Void){
        UsersAPI.get("users/\(id)", parameters: [String : AnyObject](), callback)
    }
    
    static func getProfile(callback: @escaping (User?, NSError?) -> Void){
        getUser(userId: Settings.UserId, callback)
    }
    
    static func userNotFound(email: String, companyName: String, comments: String,_ callback: @escaping (User?, NSError?) -> Void){
        let email = email as AnyObject
        let companyName = companyName as AnyObject
        let comments = comments as AnyObject
        UsersAPI.post("users/not_found", parameters: ["email": email, "company_name": companyName, "comments": comments], callback)
    }
    
    static func tryDemo(email: String, name: String, comments: String, _ callback: @escaping (ApiResponse?, NSError?) -> Void){
        let email = email as AnyObject
        let name = name as AnyObject
        let comments = comments as AnyObject
        UsersAPI.post("users/try_demo", parameters: ["email": email, "name": name, "comments": comments], callback)
    }
    
    static func recoverPassword(email: String, callback: @escaping (ApiResponse?, NSError?) -> Void){
        let email = email as AnyObject
        UsersAPI.post("recover_password", parameters: ["email": email], callback)
    }
    
    static func contact(_ comments: String, commentType: CommentType, callback: @escaping (ApiResponse?, NSError?) -> Void) -> Void{
        let comments = comments as AnyObject
        let type = commentType.rawValue as AnyObject
        UsersAPI.post("contact", parameters: ["email": comments, "type": type], callback)
    }
    
    static func updateUser(id: Int, email: String, firstName: String, lastName: String, imageUrl: String?, titleId: Int, callback: @escaping (User?, NSError?) -> Void){
        let email = email as AnyObject
        
        let firstName = firstName as AnyObject
        let lastName = lastName as AnyObject
        let titleId = titleId as AnyObject
        
        var params = ["id": "\(id)" as AnyObject , "email": email , "first_name": firstName , "last_name": lastName , "title_id": titleId ] as [String:AnyObject]
        if let iurl = imageUrl{
            if imageUrl != ""{
                params.updateValue(iurl as AnyObject, forKey: "image_url")
            }
        }
        
        UsersAPI.put("users/\(id)", parameters: params, callback)
    }
    
    static func updateUser(_ imageUrl: String, callback: @escaping (User?, NSError?) -> Void){
        let imageUrl = imageUrl as AnyObject
        UsersAPI.put("users/\(String(Settings.UserId))", parameters: ["image_url": imageUrl], callback)
    }
    
    static func editUser(_ callback: @escaping (User?, NSError?) -> Void){
        UsersAPI.get("users/\(String(Settings.UserId))/edit", parameters: [String : AnyObject](), callback)
    }
    
    static func changePassword(old: String, new: String ,_ callback: @escaping (ApiResponse?, NSError?) -> Void){
        let oldPassword = old as AnyObject
        let newPassword = new as AnyObject
        UsersAPI.post("users/change_password", parameters: ["old_password": oldPassword, "new_password": newPassword], callback)
    }
    
    //:fcm_token,:os,:device_id,:app_version
    static func updateDevice(_ callback: @escaping (ApiResponse?, NSError?) -> Void){
        
        let fcmToken = Settings.FcmToken as AnyObject
        let os = Settings.OS as AnyObject
        let deviceId = Settings.DeviceId as AnyObject
        let appVersion = Settings.AppVersion as AnyObject
        
        UsersAPI.post("users/\(Settings.UserId)/update_device", parameters: ["fcm_token" : fcmToken, "os": os, "device_id": deviceId, "app_version": appVersion], callback)
        
    }
    
    static func updateFirebaseToken(_ callback: @escaping (User?, NSError?) -> Void){
        UsersAPI.get("users/firebase_token", parameters: [String : AnyObject](), callback)
        
    }
    
    
}
