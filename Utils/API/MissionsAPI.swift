//
//  MissionsAPI.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 5/26/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import Alamofire
import AlamofireObjectMapper
import ObjectMapper
import Foundation

open class MissionsAPI: BaseAPI{
    
    static func getMissions(userId: Int, campaignId: Int, callback: @escaping ([Mission]?, NSError?) -> Void){
        
        let uId = "\(userId)" as AnyObject
        let cId = "\(campaignId)" as AnyObject
        MissionsAPI.get("missions" , parameters: ["id": uId, "campaign_id": cId], callback)
    }
    
    static func getMission(missionId: Int, callback: @escaping (Mission?, NSError?) -> Void){
        
        let mId = "\(missionId)"
        MissionsAPI.get("missions/\(mId)", parameters: [String:AnyObject](), callback)
    }
    
    static func getAnswer(missionId: Int, answerId: Int, callback: @escaping (Mission?, NSError?) -> Void){
        
        let mId = "\(missionId)"
        let aId = "\(answerId)"
        MissionsAPI.get("missions/\(mId)/answers/\(aId)", parameters: [String:AnyObject](), callback)
    }
    
    static func getMissionAnswers(missionId: Int, callback: @escaping ([Answer]?, NSError?) -> Void){
        
        let mId = "\(missionId)"
        MissionsAPI.get("missions/\(mId)/answers", parameters: [String:AnyObject](), callback)
    }
    
    static func getInfo(missionId: Int, callback: @escaping (Mission?, NSError?) -> Void){
        
        let mId = "\(missionId)"
        MissionsAPI.get("missions/\(mId)/info", parameters: [String:AnyObject](), callback)
    }
    
    static func acceptMission(missionId: Int, callback: @escaping (Mission?, NSError?) -> Void){
        
        let mId = "\(missionId)"
        MissionsAPI.post("missions/\(mId)/accept", parameters: [String:AnyObject](), callback)
    }
    
    
    static func finishMission(_ mission: Mission, callback: @escaping (MissionFinishedResponse?, NSError?) -> Void){
        let mId = mission.id!
        let json = mission.toJSON()
        print("jsonjson")
        print(json)
        MissionsAPI.post("missions/\(mId)/finish", parameters: json as [String : AnyObject], callback)
    }
    
}

