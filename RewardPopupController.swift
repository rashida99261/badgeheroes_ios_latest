//
//  RegardPopupController.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 8/23/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import UIKit

class RewardPopupController: BModalViewController {
    
   // @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var imageView: UIImageView!
  //  @IBOutlet weak var creditsLabel: UILabel!
   // @IBOutlet weak var creditsIcon: UILabel!
    
     var missions: [Mission] = [Mission]()
    
    @IBOutlet weak var imgViewBg: UIView!
    @IBOutlet weak var imgWaves: UIImageView!
    @IBOutlet weak var imgMissionOuter: UIImageView!
    @IBOutlet weak var imgCelebbg: UIImageView!
    @IBOutlet weak var progressViewBg: UIView!
    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet weak var campaignName: UILabel!
    
 //   @IBOutlet weak var innerCelebView1: UIView!
  //  @IBOutlet weak var innerCelebView2: UIView!
    @IBOutlet weak var lblCelebCreditMission: UILabel!
    @IBOutlet weak var lblCelebExpMission: UILabel!
    
    
 //   @IBOutlet var backgroundView: UIView!
    
    @IBOutlet weak var continueButton: UIButton!
    
   // var imageUrl: String!
    var credits: Int!
    var fromLocal = false
    var titleMessage: String!
    
    
    var modalCanceled: (() -> Void)?
    
    var originalMarginTopConstant: CGFloat = 0
    
    override func viewDidLoad() {
        self.modalPresentationStyle = .overCurrentContext
        self.view.backgroundColor = GlobalBgColor
        
        
        imgWaves.image = imgWaves.image?.withRenderingMode(.alwaysTemplate)
        imgWaves.tintColor = GlobalBgColor!.darker(by: 13)
        
        
        continueButton.backgroundColor = GlobalBgColor!.darker(by: 13)
        
        imgCelebbg.image = imgCelebbg.image?.withRenderingMode(.alwaysTemplate)
        imgCelebbg.tintColor = GlobalBgColor?.lighter(by: 13)

        
        imgMissionOuter.image = imgMissionOuter.image?.withRenderingMode(.alwaysTemplate)
        imgMissionOuter.tintColor = GlobalBgColor?.lighter(by: 13)

        
        
        self.campaignName.text = titleMessage.uppercased()
        self.progressViewBg.layer.cornerRadius = 20
        self.progressViewBg.clipsToBounds = true
        self.progressBar.progress = Float(missions.count) / Float(missions.count)
        self.progressBar.progressTintColor = GlobalBgColor
        self.progressBar.layer.cornerRadius = 4
        self.progressBar.layer.masksToBounds = true
        
        
//        innerCelebView1.layer.cornerRadius = 17
//        innerCelebView1.layer.masksToBounds = true
//
//        innerCelebView2.layer.cornerRadius = 17
//        innerCelebView2.layer.masksToBounds = true
        
        if let passExp = UserDefaults.standard.value(forKey: "passExp")
        {
            self.lblCelebExpMission.text = "\(passExp)"
        }
        
        if let passCredit = UserDefaults.standard.value(forKey: "passCredit")
        {
            self.lblCelebCreditMission.text = "\(passCredit)"
        }
        
        
        
        
        
        
//        if let u = imageUrl{
//            if fromLocal{
//                self.imageView.image = UIImage.init(named: u)
//            }else{
//                self.imageView.kf.setImage(with: URL(string: u))
//            }
//
//        }
        
//        if credits > 0{
//            self.creditsLabel.text = "+ \(credits!) créditos"
//        }else{
//            self.creditsIcon.isHidden = true
//            self.creditsLabel.isHidden = true
//        }
//        
//        self.popupView.layer.cornerRadius = 10
//        self.popupView.layer.masksToBounds = true
//        
//        backgroundView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
//        self.view.bringSubviewToFront(backgroundView)
        
    }
    
    @IBAction func backgroundViewTapped(_ sender: UITapGestureRecognizer) {
        self.modalCanceled?()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        UIView.animate(withDuration: 5, delay: 0.0, options: UIView.AnimationOptions(), animations:  {
               // self.marginTopConstraint.constant = self.originalMarginTopConstant
        }, completion: nil)
        
    }
    
    @IBAction func btnListoTap(_ sender: UIButton) {
        
       // let bCampaing = self.storyboard?.instantiateViewController(withIdentifier: "BCampaignsViewController") as! BCampaignsViewController
       // self.navigationController?.pushViewController(bCampaing, animated: true)
        
        //self.dismiss(animated: true, completion: nil)
       // self.navigationController?.popViewController(animated: true)
        self.navigationController?.popToRootViewController(animated: true)
        
    }
    
    

}
