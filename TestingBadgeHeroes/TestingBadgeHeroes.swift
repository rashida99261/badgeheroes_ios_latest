//
//  TestingBadgeHeroes.swift
//  TestingBadgeHeroes
//
//  Created by Max Findel on 6/28/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import XCTest

class TestingBadgeHeroes: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        //XCUIApplication().launch()
        let app = XCUIApplication()
//        setupSnapshot(app)
        app.launch()
        
        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        
        
        
        let app = XCUIApplication()
        snapshot("00LoaderScreen")
        let element = app.children(matching: .window).element(boundBy: 0).children(matching: .other).element
        element.swipeLeft()
        element.swipeLeft()
        element.swipeLeft()
        element.swipeLeft()
        
        
        
        let ingresaTuCorreoDeEmpresaTextField = app.textFields["Ingresa tu correo de empresa"]
        ingresaTuCorreoDeEmpresaTextField.tap()
        ingresaTuCorreoDeEmpresaTextField.typeText("npsegura1@gmail.com")
      
        
        let ingresarButton = app.buttons["INGRESAR"]
        ingresarButton.tap()
        
        let contraseASecureTextField = app.secureTextFields["Contraseña"]
        contraseASecureTextField.tap()
        contraseASecureTextField.typeText("nicole")
        ingresarButton.tap()
        app.buttons["CONTINUAR"].tap()
        
        let tabBarsQuery = app.tabBars
        
        
        tabBarsQuery.buttons["Perfil"].tap()
        sleep(2)
        snapshot("01Profile")
        tabBarsQuery.buttons["Actividad"].tap()
        sleep(2)
        snapshot("02Activity")
        tabBarsQuery.buttons["Campañas"].tap()
        sleep(2)
        snapshot("03Campaigns")
        tabBarsQuery.buttons["Mercado"].tap()
        sleep(2)
        snapshot("04Market")
        
        
        
    }
    
}
