//
//  Events.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 7/22/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import Foundation

enum Events : String {
    case LOGO_CHANGED = "logoChanged"
}